require 'test_helper'

class AccountSettingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @account_setting = account_settings(:one)
  end

  test "should get index" do
    get account_settings_url
    assert_response :success
  end

  test "should get new" do
    get new_account_setting_url
    assert_response :success
  end

  test "should create account_setting" do
    assert_difference('AccountSetting.count') do
      post account_settings_url, params: { account_setting: { articles_private: @account_setting.articles_private, branding_company_name: @account_setting.branding_company_name, branding_custom_css: @account_setting.branding_custom_css, branding_logo: @account_setting.branding_logo, tickets_assignee: @account_setting.tickets_assignee, tickets_auto_close: @account_setting.tickets_auto_close, tickets_auto_close_days: @account_setting.tickets_auto_close_days, tickets_private: @account_setting.tickets_private, tickets_require_valid_support: @account_setting.tickets_require_valid_support } }
    end

    assert_redirected_to account_setting_url(AccountSetting.last)
  end

  test "should show account_setting" do
    get account_setting_url(@account_setting)
    assert_response :success
  end

  test "should get edit" do
    get edit_account_setting_url(@account_setting)
    assert_response :success
  end

  test "should update account_setting" do
    patch account_setting_url(@account_setting), params: { account_setting: { articles_private: @account_setting.articles_private, branding_company_name: @account_setting.branding_company_name, branding_custom_css: @account_setting.branding_custom_css, branding_logo: @account_setting.branding_logo, tickets_assignee: @account_setting.tickets_assignee, tickets_auto_close: @account_setting.tickets_auto_close, tickets_auto_close_days: @account_setting.tickets_auto_close_days, tickets_private: @account_setting.tickets_private, tickets_require_valid_support: @account_setting.tickets_require_valid_support } }
    assert_redirected_to account_setting_url(@account_setting)
  end

  test "should destroy account_setting" do
    assert_difference('AccountSetting.count', -1) do
      delete account_setting_url(@account_setting)
    end

    assert_redirected_to account_settings_url
  end
end
