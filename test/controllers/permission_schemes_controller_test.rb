require 'test_helper'

class PermissionSchemesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @permission_scheme = permission_schemes(:one)
  end

  test "should get index" do
    get permission_schemes_url
    assert_response :success
  end

  test "should get new" do
    get new_permission_scheme_url
    assert_response :success
  end

  test "should create permission_scheme" do
    assert_difference('PermissionScheme.count') do
      post permission_schemes_url, params: { permission_scheme: { name: @permission_scheme.name } }
    end

    assert_redirected_to permission_scheme_url(PermissionScheme.last)
  end

  test "should show permission_scheme" do
    get permission_scheme_url(@permission_scheme)
    assert_response :success
  end

  test "should get edit" do
    get edit_permission_scheme_url(@permission_scheme)
    assert_response :success
  end

  test "should update permission_scheme" do
    patch permission_scheme_url(@permission_scheme), params: { permission_scheme: { name: @permission_scheme.name } }
    assert_redirected_to permission_scheme_url(@permission_scheme)
  end

  test "should destroy permission_scheme" do
    assert_difference('PermissionScheme.count', -1) do
      delete permission_scheme_url(@permission_scheme)
    end

    assert_redirected_to permission_schemes_url
  end
end
