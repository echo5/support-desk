require 'test_helper'

class Admin::RulesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_rule = admin_rules(:one)
  end

  test "should get index" do
    get admin_rules_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_rule_url
    assert_response :success
  end

  test "should create admin_rule" do
    assert_difference('Admin::Rule.count') do
      post admin_rules_url, params: { admin_rule: {  } }
    end

    assert_redirected_to admin_rule_url(Admin::Rule.last)
  end

  test "should show admin_rule" do
    get admin_rule_url(@admin_rule)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_rule_url(@admin_rule)
    assert_response :success
  end

  test "should update admin_rule" do
    patch admin_rule_url(@admin_rule), params: { admin_rule: {  } }
    assert_redirected_to admin_rule_url(@admin_rule)
  end

  test "should destroy admin_rule" do
    assert_difference('Admin::Rule.count', -1) do
      delete admin_rule_url(@admin_rule)
    end

    assert_redirected_to admin_rules_url
  end
end
