require 'test_helper'

class ChannelItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @channel_item = channel_items(:one)
  end

  test "should get index" do
    get channel_items_url
    assert_response :success
  end

  test "should get new" do
    get new_channel_item_url
    assert_response :success
  end

  test "should create channel_item" do
    assert_difference('ChannelItem.count') do
      post channel_items_url, params: { channel_item: {  } }
    end

    assert_redirected_to channel_item_url(ChannelItem.last)
  end

  test "should show channel_item" do
    get channel_item_url(@channel_item)
    assert_response :success
  end

  test "should get edit" do
    get edit_channel_item_url(@channel_item)
    assert_response :success
  end

  test "should update channel_item" do
    patch channel_item_url(@channel_item), params: { channel_item: {  } }
    assert_redirected_to channel_item_url(@channel_item)
  end

  test "should destroy channel_item" do
    assert_difference('ChannelItem.count', -1) do
      delete channel_item_url(@channel_item)
    end

    assert_redirected_to channel_items_url
  end
end
