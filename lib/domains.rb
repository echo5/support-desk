class Subdomain
  def self.matches?(request)
  	![ENV['APP_HOST'], 'localhost:3000'].include?(request.host_with_port)
  end
end

class RootDomain 
  @subdomains = ["www"]

  def self.matches?(request)
    @subdomains.include?(request.subdomain) || request.subdomain.blank?
  end
end