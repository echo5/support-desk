require 'omniauth-oauth2'
require 'envato/api'

module OmniAuth
  module Strategies
    class Envato < OmniAuth::Strategies::OAuth2

      option :client_options, {
        :site          => 'https://api.envato.com',
        :authorize_url => 'https://api.envato.com/authorization?response_type=code',
        :token_url     => '/token',
      }

      def build_access_token
        options.token_params.merge!(
          :code => request.params['code'],
          )
        super
      end

      
      uid { user_info[:username] }

      info do
        {
          :nickname => user_info[:username],
          :email => user_info[:email],
        }
      end

      # def callback_url
      #   # Rails.application.routes.url_helpers.user_envato_omniauth_callback_url(:host => ENV['APP_HOST'])
      # end

      def authorize_params
        super.tap do |params|
          %w[display scope auth_type].each do |v|
            if request.params[v]
              params[v.to_sym] = request.params[v]
            end
          end
        end
      end

      def user_info
        @user_info ||= {
          username: ::Envato::API.get('v1/market/private/user/username.json', access_token.token)['username'],
          email: ::Envato::API.get('v1/market/private/user/email.json', access_token.token)['email'],
        }
      end

      def token
        @token ||= ::Envato::API.get_access_token(request.params['code'])
      end

    end
  end
end