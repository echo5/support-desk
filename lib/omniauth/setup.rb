include ApplicationHelper

odule OmniAuth
  class Setup
    def run
      
      FACEBOOK_PROC = lambda do |env|
        req = Rack::Request.new(env)
        # Note client_id & client_secret for Facebook
        abort current_tenant.inspect
        # settings = Account.find_by_url(env)
        env['omniauth.strategy'].options[:client_id] = current_domain(req).settings.facebook_app_id
        env['omniauth.strategy'].options[:client_secret] = current_domain(req).settings.facebook_app_secret
      end
      
      # Note the below block is different for a Rails app
      Rails.application.config.middleware.use OmniAuth::Builder do
        provider :facebook, setup: FACEBOOK_PROC
        provider :facebook, "500515486964015", "dec77e67611efc7b3afe6cbc75b83198"
        provider :envato, ENV['ENVATO_CLIENT_ID'], ENV['ENVATO_API_KEY'] #client id
      end
    end
  end
end