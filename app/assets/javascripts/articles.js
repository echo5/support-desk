
/**
 * Section sorting
 */
$(document).on('turbolinks:load', function() {
	if ($('#sections.sort').length) {
		var item_id = $('#sections').data('item-id');

		sortable('#sections', {
		  placeholderClass: 'ghost',
		  placeholder: '<div class="col-md-4">&nbsp;</div>'
		});

		sortable('#sections')[0].addEventListener('sortupdate', function(e) {
			item_order = []
			$(e.detail.endparent).children().each( function() {
				item_order.push($(this).data('id'));
			});
		    $.ajax({
		        type: "PUT",
		        url: '/sections/sort',
		        data: { order: item_order }
		    });
		});
	}

	if ($('.articles.sort').length) {

		sortable('.articles', {
			connectWith: 'articles-connected',
			placeholderClass: 'ghost',
			placeholder: '<div class="article">&nbsp;</div>'
		});

		sortable('.articles')[0].addEventListener('sortupdate', function(e) {
			item_order = []
			var section_id = $(e.detail.endparent).data('section-id');
			var article_id = $(e.detail.item).data('id');
			$(e.detail.endparent).children().each( function() {
				item_order.push($(this).data('id'));
			});
			// Moved list, update parent section
			if (e.detail.endparent !== e.detail.startparent) {
				$.ajax({
				    type: "PUT",
				    url: '/items/' + item_id + '/move_article',
				    data: { 
				    	section_id: section_id,
				    	article_id: article_id
				    },
				    success: reorderSections
				});
			} else {
				reorderSections();
			}

		    function reorderSections() {
		    	$.ajax({
		    	    type: "PUT",
		    	    url: '/sections/' + section_id + '/sort_articles',
		    	    data: { order: item_order }
		    	});
		    }

		});
	}

});


/**
 * Article sorting
 */
$(document).on('turbolinks:load', function() {


});

