// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require payola
//= require jquery_ujs
//= require turbolinks
//= require tether
//= require bootstrap/util
//= require bootstrap/alert
//= require bootstrap/collapse
//= require bootstrap/dropdown
//= require bootstrap/modal
//= require bootstrap/tab
//= require bootstrap/button
//= require bootstrap/tooltip
//= require highlight.pack
//= require cocoon
//= require editor
//= require trix
//= require quill
//= require dropzone
//= require selectize
//= require sortablejs/Sortable
//= require jquery-minicolors/jquery.minicolors
//= require navigation
//= require tickets
//= require search
//= require account_settings
//= require plans
//= require forms
//= require subscriptions
//= require payola
//= require channels
//= require modals
//= require attachments

$(document).on('turbolinks:load', function() {

  $("tr[data-url] td:first-child").click( function (e) {
    Turbolinks.visit($(this).parent().data('url'));
  });

});
