window.selectizeForms = function() {
  $('select.select-tags').selectize({
      plugins: ['remove_button'],
      delimiter: ',',
      persist: false,
      create: false
  });

  $('select:not(.select-tags, [class^="ql"])').selectize({ 
      allowEmptyOption: true,
      render: {
        item: function(item, escape) {
            return '<div>' + (item.html ? item.html : escape(item.text)) +'</div>';
        },
        option: function(item, escape) {
            return '<div>' + (item.html ? item.html : escape(item.text)) +'</div>';
        }
      },
      onDropdownClose: function(dropdown) {
        console.log($(dropdown).prev().find('input'));
        $(dropdown).prev().find('input').blur();
      }
  });
}

$(document).on('turbolinks:load', function() {
  window.selectizeForms();
});
// Cleanup/destroy selectize
$(document).on('turbolinks:change turbolinks:before-cache', function() {
  cleanUpSelectize();
});

function cleanUpSelectize() {
  $('select').each(function() {
    if ($(this)[0].selectize) { // requires [0] to select the proper object
       $(this)[0].selectize.destroy(); // destroys selectize()
    }
  });
}

/**
 * Create modal
 */
function doModal(heading, formContent) {
    html =  '<div id="dynamic-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
    html += '<div class="modal-dialog">';
    html += '<div class="modal-content">';
    html += '<div class="modal-header">';
    html += '<h4>'+heading+'</h4>'
    html += '<a class="close" data-dismiss="modal">×</a>';
    html += '</div>';
    html += '<div class="modal-body">';
    html += formContent;
    html += '</div>';
    html += '<div class="modal-footer">';
    html += '<span class="btn btn-danger" id="confirm-delete">Confirm</span>';
    html += '</div>';  // content
    html += '</div>';  // dialog
    html += '</div>';  // footer
    html += '</div>';  // modalWindow
    $('body').append(html);
    // $("#dynamic-modal").modal();
    $("#dynamic-modal").modal('show');

    $('#dynamic-modal').on('hidden.bs.modal', function (e) {
        $(this).remove();
    });

}

/**
 * Custom confirmation message
 */
$(document).on('confirm', function (event) {
  var element = $(event.target);
  var message = element.data("confirm");
  doModal('Confirm', message);
  $('#confirm-delete').click( function() {
    element.data("confirm", null);
    element.trigger("click.rails");
    element.data("confirm", message);
    $("#dynamic-modal").modal('hide');    
  });
  return false;
});

$('#main').on('cocoon:after-insert', function(e, insertedItem) {
  window.selectizeForms();
});