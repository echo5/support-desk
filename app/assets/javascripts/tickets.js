/**
 * Code highlighting
 */
$(document).on('turbolinks:load', function() {
	$('pre').each(function(i, block) {
	  hljs.highlightBlock(block);
	});
});

/**
 * Filtering
 */
 $(document).on('turbolinks:load', function() {
  $(".filters :input").change(function() {
      setGetParameter($(this).attr('name'), $(this).val());
  });
 });
function setGetParameter(paramName, paramValue)
{
    var url = window.location.href;
    var hash = location.hash;
    url = url.replace(hash, '');
    if (url.indexOf(paramName + "=") >= 0)
    {
        var prefix = url.substring(0, url.indexOf(paramName));
        var suffix = url.substring(url.indexOf(paramName));
        suffix = suffix.substring(suffix.indexOf("=") + 1);
        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
        url = prefix + paramName + "=" + paramValue + suffix;
    }
    else
    {
    if (url.indexOf("?") < 0)
        url += "?" + paramName + "=" + paramValue;
    else
        url += "&" + paramName + "=" + paramValue;
    }
    Turbolinks.visit(url + hash);
}

/**
 * Add hash on tab click
 */
$(document).on('turbolinks:load', function() {
  var hash = window.location.hash;
  hash && $('ul.nav a[href="' + hash + '"]').tab('show');

  $('a[role=tab]').click(function (e) {
    history.pushState(null, null, $(this).attr('href'));
  });
});

/**
 * Change tab on state change
 */
window.addEventListener("popstate", function(e) {
  var activeTab = $('a[href="' + location.hash + '"]');
  if (activeTab.length) {
    activeTab.tab('show');
  } else {
    $('.nav-tabs a:first').tab('show');
  }
});

/**
 * Hide dependent fields
 */
 $(document).on('turbolinks:load', function() {
    $('.dependency input').click(function() {
      var dependencyId = $(this).attr('id');
      if($(this). prop("checked") == true){
        $('*[data-dependent="' + dependencyId + '"]').show();
      } else {
        $('*[data-dependent="' + dependencyId + '"]').hide();
      }
    })
 });

/** 
 * Show purchase inputs on select
 */
$(document).on('turbolinks:load', function() {
  $('.ticket-purchase').change(function() {
    $('.ticket_purchase_url').hide();
    $(this).closest('.form-group').next('.ticket_purchase_url').show();
  });
});

/**
 * Copy to clipboard
 */
 function copyToClipboard(text) {
     var $temp = $('<input>');
     $("body").append($temp);
     $temp.val(text).select();
     document.execCommand("copy");
     $temp.remove();
     addToast('Copied to clipbaord', 'success');
 }
 $(document).on('turbolinks:load', function() {
    $('.permalink').click(function(e) {
      e.preventDefault();
      copyToClipboard($(this).attr('href'));
    })
 });

 /**
  * Update form
  */
  $('.edit_ticket').on('change', function() {
    $(this).submit();
  });