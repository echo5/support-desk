$(document).on('turbolinks:load', function() {
    var searchField = $('#search_field'),
        searchResults = $('#search_results');

    searchField.keyup(function() {
         var value = $(this).val();
         if(value.length>= 3){
            ajaxSearch(value, $(this).data('item-id'));
         }
    });

    // $("body").click(function(e) {
    //     if (e.target.id == "search" || $(e.target).parents("#myDiv").size()) { 
    //         // do nothing
    //     } else { 
    //         searchResults.hide();
    //     }
    // });
});

$(document).on('click', '#search-overlay', hideSearchResults);

function hideSearchResults() {
    searchResults = $('#search_results');
    searchResults.hide();
}


function ajaxSearch(value, item_id){
    $.ajax({
        type: "GET",
        url: "/search",
        data: { 
            q: value,
            item_id: item_id
        },
        success: function(result) {
        }
    });
}