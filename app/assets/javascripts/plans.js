$(document).on('turbolinks:load', function() {

	var quantityField = $('.subscription_quantity');

	quantityField.change(function(){
		var priceField = $(this).closest('.plan').find('h4');
		var basePrice = priceField.data('base-price');
		var total = basePrice * $(this).val();
		priceField.html('$' + total);
		$(this).next('.agent-noun').text(($(this).val() == 1) ? "agent" : "agents");
	});


	quantityField.change();

});