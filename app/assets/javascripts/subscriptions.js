(function() {

  /**
   * Credit card form
   */
   function prepareCardForms() {
     $('.exp-date').payment('formatCardExpiry');
     $('input[data-stripe="number"]').payment('formatCardNumber');
     $('input[data-stripe="cvc"]').payment('formatCardCVC');

     $('.exp-date').on('keyup', function() {
         var e = $('.exp-date').first();
         var out = $.payment.cardExpiryVal(e.val());
         $('input[data-stripe="exp_month"]').val(out.month);
         $('input[data-stripe="exp_year"]').val(out.year);
     });

     $('.card-number').on('keyup', function() {
       var e = $('.card-number').first();
       var type = $.payment.cardType(e.val());
       var img = "credit.png";
       switch(type) {
           case "visa":
             img = "visa.png";
             break;
           case "mastercard":
             img = "mastercard.png";
             break;
           case "discover":
             img = "discover.png";
             break;
           case "amex":
             img = "amex.png";
             break;
       }
       e.css('background-image', 'url(/assets/creditcards/' + img + ')');
     });
   }
   $(document).on('turbolinks:load', function() {
      prepareCardForms();
   });

//   $(document).ready(function(){
//     prepareCardForms();
//   });

  /**
   * Subscription/change button
   */
  $('.subscription-btn').click(function() {
    var quantity = $(this).siblings('.subscription_quantity').val();
    var planId = $(this).siblings('.subscription_plan_id').val();
    var planName = $(this).siblings('.subscription_plan_name').val();
    $('#subscription-form').attr('data-payola-plan-type', planName);
    $('#subscription-form').attr('data-payola-plan-id', planId);
    $('#subscription-form').find('.quantity').val(quantity);
  });

  $(document).on('submit', '#update-card-form', function(e) {
    var updateCardForm = $('#update-card-form');
    e.preventDefault();
    if (!PayolaOnestepSubscriptionForm.validateForm(updateCardForm)) {
        return false;
    }
    $('.payola-spinner').show();
    updateCardForm.find(':submit').prop('disabled', true);
    Stripe.card.createToken($(this), function(status, response) {
      if (response.error) {
          PayolaOnestepSubscriptionForm.showError(updateCardForm, response.error.message);
      } else {
          var action = $(updateCardForm).attr('action');
          updateCardForm.append($('<input type="hidden" name="stripeToken">').val(response.id));
          updateCardForm.append(PayolaOnestepSubscriptionForm.authenticityTokenInput());
          $.ajax({
              type: "POST",
              url: action,
              data: updateCardForm.serialize(),
              success: function(data) { 
                console.log(data);
                poll(updateCardForm, 60, data.guid, 'payola'); },
              error: function(data) { PayolaOnestepSubscriptionForm.showError(updateCardForm, jQuery.parseJSON(data.responseText).error); }
          });
      }
    });
    return false;
  });

  function poll(form, num_retries_left, guid, base_path) {
      if (num_retries_left === 0) {
          PayolaOnestepSubscriptionForm.showError(form, "This seems to be taking too long. Please contact support and give them transaction ID: " + guid);
      }
      var handler = function(data) {
          if (data.status === "active") {
              window.location = base_path + '/confirm_subscription/' + guid;
          } else {
              setTimeout(function() { PayolaOnestepSubscriptionForm.poll(form, num_retries_left - 1, guid, base_path); }, 500);
          }
      };
      var errorHandler = function(jqXHR){
          PayolaOnestepSubscriptionForm.showError(form, jQuery.parseJSON(jqXHR.responseText).error);
      };
      
      if (typeof guid != 'undefined') {
          $.ajax({
              type: 'GET',
              dataType: 'json',
              url: base_path + '/subscription_status/' + guid,
              success: handler,
              error: errorHandler
          });
      }
  }


})(jQuery);