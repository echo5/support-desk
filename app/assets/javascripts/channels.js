
window.syncChannels = function() {
  $("#channel-items").addClass('loading');
	$.ajax({
    url: "/admin/channels/sync",
    cache: false,
    success: function(html){
		  $("#channel-items").removeClass('loading');
      $("#channel-items").append(html);
    }
	});
}

$('#sync-channels-btn').click(function() {
	window.syncChannels();
});