/**
 * Create or find modal, add content, and show
 */
function renderModal(id = '', content = '') {
	var modal = $('#' + id);
	if (!modal.length) {
		modal = $('body').append('<div id="' + id + '" class="modal"><div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + content + '</div></div></div></div>');
	} else {
		modal.find('modal-body').html(content);
	}
	$('#' + id).modal('show');
}

/**
 * Add toast notification with class
 */
function addToast(content, classes) {
	$alert = $('<div class="alert alert-' + classes + '">' + content + '</div>');
	$('#notifications').append($alert);
	$alert.delay(5000).hide(500); 
}

/**
 * Hide notifications on click
 */
$(document).on('click', '#notifications .alert', function() {
	$(this).hide();
});

$(document).on('turbolinks:load', function() {
	$('#notifications .alert').each(function() {
		$(this).delay(5000).hide(500);
	});
});