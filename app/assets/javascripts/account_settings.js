/**
 * Color picker
 */
$(document).on('turbolinks:load', function() {
	var settings = {
		opacity: true,
		format: 'rgb',
		position: 'bottom left',
		theme: 'custom',
	}
	$('.color-picker').minicolors(settings);
});