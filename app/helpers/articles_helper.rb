module ArticlesHelper
	def article_item_url(article, item = false)
		item ||= @item
		if item
			article_url(article) + '?item=' + item.id.to_s
		else
			article_url(article)
		end
	end
end
