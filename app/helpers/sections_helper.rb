module SectionsHelper
	def section_item_url(section)
		if @item
			section_url(section) + '?item=' + @item.id.to_s
		else
			section_url(section)
		end
	end
end
