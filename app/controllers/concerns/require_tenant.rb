module RequireTenant
  extend ActiveSupport::Concern

  included do
	before_action :redirect_to_branding_domain
	before_action :require_tenant
  end

  def redirect_to_branding_domain
  	# Allow default subdomain if locked out
  	return if request.path.starts_with?('/login')
		if current_tenant && current_tenant.settings.branding_domain && current_tenant.settings.branding_domain != request.host
  		redirect_to "#{request.protocol}#{current_tenant.settings.branding_domain}#{request.fullpath}", :status => :moved_permanently unless current_user && current_user.is_agent?
		end
	end

  def require_tenant
		if !current_tenant
  		redirect_to main_app.root_path
		elsif current_user && current_user.is_agent?
			if current_tenant.is_trialing?
				return
			elsif current_tenant.subscription.nil?
				flash[:error] = 'Your free trial has ended.'
			  redirect_to subscription_path
			elsif current_tenant.subscription.current_period_end < Time.now
				# flash[:error] = 'Your subscription has expired.'
			  # redirect_to subscription_path
		  end
		end
  end

end