module NoTenant
  extend ActiveSupport::Concern

  included do
	before_action :block_tenant
  end

  def block_tenant
  	if current_tenant
  		if request.env['PATH_INFO'] == '/'
	  		# render 'items/index'
  		else
  			render '404'
		end
	end
  end

end