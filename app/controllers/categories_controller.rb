class CategoriesController < ApplicationController
  include RequireTenant
  before_action :set_category, only: [:show, :update, :destroy, :edit]

  # GET /categories/1
  # GET /categories/1.json
  def show
    @sections = Section.where(category_id: @category.id)
    @categories = policy_scope(Category).includes(:sections)    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

end
