class ArticlesController < ApplicationController
  include RequireTenant
  after_action except: [:index] { authorize @article }
  after_action only:   [:index] { authorize @articles }
  before_action :set_article, only: [:show, :edit, :update, :destroy, :move]

  # GET /articles
  # GET /articles.json
  def index
    @article = Article.new
    @articles = Article.includes(:section).filter(filtering_params(params)).paginate(:page => params[:page])
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
    @attachments = @article.attachments
    if params.has_key?(:item)
      @item = Item.find(params[:item])
      @sections = Section.with_published_articles_for_item(@item)
    end
  end

  # GET /articles/new
  def new
    @article = Article.new
    @article.items.build
  end

  # GET /articles/1/edit
  def edit
    session[:return_to] = request.referer
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(article_params)

    respond_to do |format|
      if @article.save
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update

    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to session[:return_to], notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        flash[:errors] = @article.errors.full_messages.join('<br>')
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /articles/1/move
  def move

    @article.move_to! params[:position]
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:title, :status, :section_id, :content, :item_ids => [])
    end

    # A list of the param names that can be used for filtering
    def filtering_params(params)
      params.slice(:title, :section, :item)
    end
end
