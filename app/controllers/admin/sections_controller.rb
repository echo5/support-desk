class Admin::SectionsController < ApplicationController
  include RequireTenant
  before_action :set_section, only: [:show, :edit, :update, :destroy, :move]
  layout 'admin'  

  # GET /sections
  # GET /sections.json
  def index
    @section = Section.new
    @sections = Section.filter(filtering_params(params)).paginate(:page => params[:page])
  end

  # GET /sections/1
  # GET /sections/1.json
  def show
      @categories = Category.all
      @section = Section.includes(:articles).find(params[:id])
  end

  # PUT /sections/sort
  def sort
    params[:order].each_with_index do |value,index|
      next if value == "NULL"
      Section.find(value).update_attribute(:sort_order,index)
    end
    render :success => true
  end

  # PUT /sections/1/sort_articles
  def sort_articles
    params[:order].each_with_index do |value,index|
      next if value == "NULL"
      @article_item = ArticlesItem.find_by(section_id: params[:id], article_id: value.to_i)
      @article_item.sort_order = index
      if @article_item.save
        # render :success => true
      end
    end
  end

  # POST /sections/1/move
  def move
    @section.move_to! params[:position]
  end

  # GET /sections/new
  def new
    @section = Section.new
    session[:return_to] = request.referer
  end

  # GET /sections/1/edit
  def edit
    # @articles = Article.where(section_id: @section.id).ordered_by_position_asc
  end

  # POST /sections
  # POST /sections.json
  def create
    @section = Section.new(section_params)

    respond_to do |format|
      if @section.save
        format.html { redirect_to session[:return_to], notice: 'Section was successfully created.' }
        format.json { render :show, status: :created, location: @section }
      else
        format.html { render :new }
        format.json { render json: @section.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sections/1
  # PATCH/PUT /sections/1.json
  def update
    if @section.update(section_params)
      flash[:success] = 'Section updated.'
    else
      flash[:errors] = @section.errors.full_messages.join('<br>')
    end
    redirect_to :back
  end

  # DELETE /sections/1
  # DELETE /sections/1.json
  def destroy
    @section.destroy
    respond_to do |format|
      format.html { redirect_to sections_url, notice: 'Section was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_section
      @section = Section.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def section_params
      params.require(:section).permit(:name, :sort_order, :item_id)
    end

    # A list of the param names that can be used for filtering
    def filtering_params(params)
      params.slice(:title)
    end
end
