class Admin::CategoriesController < ApplicationController
  layout 'admin'
  include RequireTenant
  # load_and_authorize_resource
  before_action :set_category, only: [:show, :update, :destroy, :edit]
  after_action except: [:index] { authorize @category }
  after_action only:   [:index] { authorize @categories }

  # GET /categories
  # GET /categories.json
  def index
    @category = Category.new
    @categories = policy_scope(Category).includes(:sections).paginate(:page => params[:page])
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
    @sections = Section.where(category_id: @category.id)
    @categories = policy_scope(Category).includes(:sections)    
  end

  # PUT /categories/1/move_article
  def move_article
    @article_category = ArticlesCategory.find_by(category_id: params[:id], article_id: params[:article_id].to_i)
    @article_category.section_id = params[:section_id].to_i
    if @article_item.save
      render :success => true
    end
  end

  # GET /categories/new
  def new
    @category = Category.new
  end

  # GET /categories/1/edit
  def edit
    @category = Category.find(params[:id])
  end

  # POST /categories
  # POST /categories.json
  def create
    @category = Category.new(category_params)

    respond_to do |format|
      if @category.save
        format.html { redirect_to show_category_path(@category), notice: 'Category was successfully created.' }
        format.json { render :show, status: :created, location: @category }
      else
        format.html { render :new }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /categories/sync
  def sync
    envato_items = current_user.get_envato_items
    envato_items.each do |envato_item|
      @categories = Category.find_or_create_by(external_id: envato_item['id'], source: 'envato') do |category|
        category.name = envato_item['name']
        category.slug = envato_item['name'].parameterize
        category.image = open(envato_item['previews']['icon_with_landscape_preview']['landscape_url'])
      end
    end
    if @categories.errors.any?
      flash[:error] = @categories.errors.join("<br>")
    else
      flash[:success] = 'categories synced.'
    end
    redirect_to categories_account_settings_path
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
  def update
    respond_to do |format|
      if @category.update(category_params)
        format.html { redirect_to show_category_path(@category), notice: 'Category was successfully updated.' }
        format.json { render :show, status: :ok, location: @category }
      else
        format.html { render :edit }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @category.destroy
    respond_to do |format|
      format.html { redirect_to root_path, notice: 'Category was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.fetch(:category, {}).permit(:name, :slug, :external_id, :source, :status, :rule_set_ids => [])
    end
end
