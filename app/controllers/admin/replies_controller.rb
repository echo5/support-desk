class Admin::RepliesController < RepliesController
  layout 'admin'
  before_filter :restrict_to_agents

  protected

    def restrict_to_agents
      redirect_to root_path if !current_user || !current_user.is_agent?
    end

end