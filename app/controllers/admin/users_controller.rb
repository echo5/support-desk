class Admin::UsersController < ApplicationController
	layout 'admin'
  include RequireTenant
	before_action :set_user, only: [:show, :edit, :update, :destroy, :sync_envato_purchases]

	# GET /users
	# GET /users.json
	def index
		@user = User.new
		@users = User.filter(filtering_params(params)).paginate(:page => params[:page]).paginate(:page => params[:page])		
	end

	# GET /users/1
	# GET /users/1.json
	def show
	end

	# GET /users/new
	def new
	  @user = User.new
	end

	# GET /users/1/edit
	def edit
	end

	# POST /users
	# POST /users.json
	def create
	  @user = User.new(user_params)

	  respond_to do |format|
	    if @user.save
	      format.html { redirect_to @user, notice: 'User created.' }
	      format.json { render :show, status: :created, location: @user }
	    else
	      format.html { render :new }
	      format.json { render json: @user.errors, status: :unprocessable_entity }
	    end
	  end
	end

	# PATCH/PUT /users/1
	# PATCH/PUT /users/1.json
	def update
	  respond_to do |format|
	    if @user.update(user_params)
	      format.html { redirect_to @user, notice: 'User updated.' }
	      format.json { render :show, status: :ok, location: @user }
	    else
	    	flash[:error] = @user.errors.full_messages.join('<br>')
	      format.html { render :show }
	      format.json { render json: @user.errors, status: :unprocessable_entity }
	    end
	  end
	end

	# DELETE /users/1
	# DELETE /users/1.json
	def destroy
	  @user.destroy
	  respond_to do |format|
	    format.html { redirect_to users_url, notice: 'User deleted.' }
	    format.json { head :no_content }
	  end
	end

  # GET /users/1/sync_envato_purchases
  def sync_envato_purchases
    if @user.envato
      user_datum = UserDatum.find_or_create_by(key: 'envato_purchases', user: @user)
			items = Item.where(source: 'envato').select(:id, :external_id, :name)
      envato_purchases = current_user.get_envato_purchases
      purchases = []
      envato_purchases['results'].each do |purchase| 
        item = items.select { |item| item.external_id ==  purchase['item']['id']}.first
        next if !item
        p = {}
        p[:code] = purchase['code']
        p[:item_id] = item.id
        p[:item_name] = item.name
        p[:envato_id] = purchase['item']['id']
        p[:sold_at] = purchase['sold_at']
        p[:amount] = purchase['amount']
        p[:license] = purchase['license']
        p[:support_amount] = purchase['support_amount']
        p[:supported_until] = purchase['supported_until']
        p[:code] = purchase['code']
        purchases << p
      end
      user_datum.info = purchases
      user_datum.save
      flash[:success] = 'Envato purchases synced.'
      redirect_to user_path(current_user, :anchor => "purchases")
    else
      redirect_to user_envato_omniauth_authorize_path
    end
  end

	private
	  # Use callbacks to share common setup or constraints between actions.
	  def set_user
			@user = User.includes(:data, :identities, memberships: [role: :resource]).find_by(username: params[:username])
	  	@tickets = @user.tickets.includes(:labels, :user).paginate(:page => params[:page])
	  	@user_settings = UserSetting.find_or_initialize_by(user_id: @user.id)
	  end

	  # Never trust parameters from the scary internet, only allow the white list through.
	  def user_params
	  	user_params = params.require(:user).permit(:username, :email, :password, :password_confirmation)
	  	# Devise blank password fix
	  	if user_params[:password].blank?
	  		user_params.delete :password
	  		user_params.delete :password_confirmation
  		end
	  	if can? :assign_roles, User
	      user_params = user_params.merge(params.require(:user).permit(:role))
    	end
    	user_params
	  end

	  # A list of the param names that can be used for filtering
	  def filtering_params(params)
	    params.slice(:username, :email, :role)
	  end

end
