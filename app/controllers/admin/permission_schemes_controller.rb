class Admin::PermissionSchemesController < ApplicationController
  before_action :set_permission_scheme, only: [:show, :edit, :update, :destroy]

  # GET /permission_schemes
  # GET /permission_schemes.json
  def index
    @permission_schemes = PermissionScheme.all
  end

  # GET /permission_schemes/1
  # GET /permission_schemes/1.json
  def show
  end

  # GET /permission_schemes/new
  def new
    @permission_scheme = PermissionScheme.new
  end

  # GET /permission_schemes/1/edit
  def edit
    @permission_scheme = PermissionScheme.includes(:permissions).find(params[:id])
  end

  # POST /permission_schemes
  # POST /permission_schemes.json
  def create
    @permission_scheme = PermissionScheme.new(permission_scheme_params)

    respond_to do |format|
      if @permission_scheme.save
        format.html { redirect_to @permission_scheme, notice: 'Permission scheme was successfully created.' }
        format.json { render :show, status: :created, location: @permission_scheme }
      else
        format.html { render :new }
        format.json { render json: @permission_scheme.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /permission_schemes/1
  # PATCH/PUT /permission_schemes/1.json
  def update
    respond_to do |format|
      if @permission_scheme.update(permission_scheme_params)
        format.html { redirect_to @permission_scheme, notice: 'Permission scheme was successfully updated.' }
        format.json { render :show, status: :ok, location: @permission_scheme }
      else
        format.html { render :edit }
        format.json { render json: @permission_scheme.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /permission_schemes/1
  # DELETE /permission_schemes/1.json
  def destroy
    @permission_scheme.destroy
    respond_to do |format|
      format.html { redirect_to permission_schemes_url, notice: 'Permission scheme was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_permission_scheme
      @permission_scheme = PermissionScheme.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def permission_scheme_params
      params.require(:permission_scheme).permit(:name)
    end
end
