class Admin::AccountSettingsController < ApplicationController
  before_action :authenticate_user!
  include RequireTenant
  layout 'admin'
  load_and_authorize_resource
  layout 'settings'
  # before_action :do_imports, only: [:update]

  # GET /settings
  # GET /settings.json
  def show
    render 'branding'
  end

  def labels
    @label = Label.new
    @labels = Label.all
  end

  def rules
    @rule_set = RuleSet.new
    @rule_set.rules.build
    @rule_sets = RuleSet.includes(:rules).all
  end

  def roles
    @role = Role.new
    @roles = Role.all
  end

  def permission_schemes
    @permission_scheme = PermissionScheme.new
    @permission_schemes = PermissionScheme.includes(:permissions).all #.group(:action)
    @permissions = Permission.where(scheme: @permission_schemes.first).group(:action, :id)
  end

  def canned_responses
    @canned_response = CannedResponse.new
    @canned_responses = CannedResponse.all
  end

  def articles
    @assignee_options = User.agents.map {|user| [user.display_name, user.id] }
  end

  # POST /settings
  # POST /settings.json
  def create
    self.update
  end

  # PATCH/PUT /settings/1
  # PATCH/PUT /settings/1.json
  def update
    respond_to do |format|
      if @account_settings.update(account_setting_params)
        flash[:success] = 'Account settings were successfully updated.'
        format.html { render :show }
        format.js { }
      else
        flash[:error] = @account_settings.errors.full_messages.join('<br>')
        format.html { redirect_to :back }
        format.js { }
      end
    end
  end

  def import_tickets
    if params[:tickets_import_file]
      @import = ImportTickets.new(file: params[:tickets_import_file])
      @import.call
      render :imports
    end
  end

  def import_users
    if params[:users_import_file]
      ImportWorker.perform_async('users', file: params[:users_import_file])
      render :imports
    end
  end

  def import_replies
    if params[:replies_import_file]
      @import = ImportReplies.new(file: params[:replies_import_file])
      @import.call
      render :imports
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_setting_params
      params.require(:account_setting).permit(
        :branding_company_name,
        :branding_logo,
        :branding_header_color,
        :branding_header_font_color,
        :branding_primary_color,
        :branding_secondary_color,
        :branding_domain,
        :branding_custom_css,
        :tickets_require_valid_support,
        :tickets_auto_close,
        :tickets_auto_close_days,
        :tickets_private,
        :tickets_auto_assignee_id,
        :articles_private,
        :envato_enabled,
        :envato_key,
        :envato_secret,
        :facebook_enabled,
        :facebook_key,
        :facebook_secret,
        :twitter_enabled,
        :twitter_key,
        :twitter_secret,
        :googleplus_enabled,
        :googleplus_key,
        :googleplus_secret,
      )
    end
end
