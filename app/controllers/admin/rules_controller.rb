class Admin::RulesController < ApplicationController
  before_action :set_admin_rule, only: [:show, :edit, :update, :destroy]

  # GET /admin/rules
  # GET /admin/rules.json
  def index
    @admin_rules = Rule.all
  end

  # GET /admin/rules/1
  # GET /admin/rules/1.json
  def show
  end

  # GET /admin/rules/new
  def new
    @admin_rule = Rule.new
  end

  # GET /admin/rules/1/edit
  def edit
  end

  # POST /admin/rules
  # POST /admin/rules.json
  def create
    @admin_rule = Rule.new(admin_rule_params)

    respond_to do |format|
      if @admin_rule.save
        format.html { redirect_to @admin_rule, notice: 'Rule was successfully created.' }
        format.json { render :show, status: :created, location: @admin_rule }
      else
        format.html { render :new }
        format.json { render json: @admin_rule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/rules/1
  # PATCH/PUT /admin/rules/1.json
  def update
    respond_to do |format|
      if @admin_rule.update(admin_rule_params)
        format.html { redirect_to @admin_rule, notice: 'Rule was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_rule }
      else
        format.html { render :edit }
        format.json { render json: @admin_rule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/rules/1
  # DELETE /admin/rules/1.json
  def destroy
    @admin_rule.destroy
    respond_to do |format|
      format.html { redirect_to admin_rules_url, notice: 'Rule was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_rule
      @admin_rule = Rule.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_rule_params
      params.fetch(:admin_rule, {})
    end
end
