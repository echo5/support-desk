class Admin::ProductsController < ApplicationController
  layout 'admin'
  before_action :set_product, except: [:index]

  # GET /products
  # GET /products.json
  def index
    @products = Product.all
  end

  # GET /products/1
  # GET /products/1.json
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  def users
    # @users = User.joins(:roles).where(roles: {resource_id: @product.id}).distinct.paginate(:page => params[:page])
    @users = User.includes(memberships: :role).joins(:users_roles).references(:users_roles).where(users_roles: { role_id: @product.roles.map(&:id)}).distinct.paginate(:page => params[:page])
    # abort @users[0].memberships.inspect
    @users_role = UsersRole.new
    @product_roles = @product.roles


    # places = Place.all.to_a
    
    # # 2nd query: load events for given places, matching the date condition
    # events = Event.where(place: places.map(&:id)).where("start_date > '#{time_in_the_future}'")
    # events_by_place_id = events.group_by(&:place_id)
    
    # # 3: manually set the association
    # places.each do |place|
    #   events = events_by_place_id[place.id] || []
    
    #   association = place.association(:events)
    #   association.loaded!
    #   association.target.concat(events)
    #   events.each { |event| association.set_inverse_instance(event) }
    # end
  end

  def update_user_roles
    @user = User.find(params[:product][:users].to_i)
    @roles = params[:product][:roles]
    @roles.each do |role_id|
      next if role_id.blank?
      role = Role.find_by id: role_id, resource_id: nil
      @user.add_role role.name, @product if role
    end
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        flash[:success] = 'Product was successfully updated.'
        format.html { redirect_to edit_admin_product_path @product }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find_by_slug!(params[:slug])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.fetch(:product, {}).permit(:name,:slug,:description,:image)
    end
end
