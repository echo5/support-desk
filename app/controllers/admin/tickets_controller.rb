class Admin::TicketsController < TicketsController
  layout 'admin'
  before_filter :restrict_to_agents

  # PATCH/PUT /admin/tickets/1
  # PATCH/PUT /admin/tickets/1.json
  def update
    if @ticket.update(ticket_params)
      flash[:success] = 'Ticket was successfully updated.'
    else
      flash[:error] = @ticket.errors.full_messages.join('<br>')
      redirect :back
    end
  end

  protected

    def restrict_to_agents
      redirect_to root_path if !current_user || !current_user.is_agent?
    end

end