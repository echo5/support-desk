class Admin::CannedResponsesController < ApplicationController
  include RequireTenant
  layout 'admin'
  before_action :set_canned_response, only: [:show, :edit, :update, :destroy]

  # GET /canned_responses
  # GET /canned_responses.json
  def index
    @canned_responses = CannedResponse.all
  end

  # GET /canned_responses/1
  # GET /canned_responses/1.json
  def show
  end

  # GET /canned_responses/new
  def new
    @canned_response = CannedResponse.new
  end

  # GET /canned_responses/1/edit
  def edit
  end

  # POST /canned_responses
  # POST /canned_responses.json
  def create
    @canned_response = CannedResponse.new(canned_response_params)

      if @canned_response.save
        flash[:success] = "Canned response created."
        # format.html { redirect_to @canned_response, notice: 'Canned response was successfully created.' }
        # format.js { }
      else
        flash[:error] = @canned_response.errors
        # format.html { render :new }
        # format.js { }
      end
  end

  # PATCH/PUT /canned_responses/1
  # PATCH/PUT /canned_responses/1.json
  def update
    respond_to do |format|
      if @canned_response.update(canned_response_params)
        flash[:success] = "Canned response was successfully updated."
        # format.html { redirect_to @canned_response, notice: 'Canned response was successfully updated.' }
        format.js { }
      else
        flash[:error] = @canned_response.errors
        format.html { render :edit }
        format.js { }
      end
    end
  end

  # DELETE /canned_responses/1
  # DELETE /canned_responses/1.json
  def destroy
    @canned_response.destroy
    flash[:success] = "Canned response deleted."
    respond_to do |format|
      # format.html { redirect_to canned_responses_url, notice: 'Canned response was successfully destroyed.' }
      format.js { }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_canned_response
      @canned_response = CannedResponse.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def canned_response_params
      params.require(:canned_response).permit(:name, :content)
    end
end
