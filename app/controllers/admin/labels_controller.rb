class Admin::LabelsController < ApplicationController
  include RequireTenant
  layout 'admin'
  before_action :set_label, only: [:show, :edit, :update, :destroy]

  # GET /labels
  # GET /labels.json
  def index
    @labels = Label.all
  end

  # GET /labels/1
  # GET /labels/1.json
  def show
  end

  # GET /labels/new
  def new
    @label = Label.new
  end

  # GET /labels/1/edit
  def edit
  end

  # POST /labels
  # POST /labels.json
  def create
    @label = Label.new(label_params)

    respond_to do |format|
      if @label.save
        flash[:success] = "Label was successfully created."
        # format.html { redirect_to @label, notice: 'Label was successfully created.' }
        format.js { }
      else
        format.html { render :new }
        format.js { }
      end
    end
  end

  # PATCH/PUT /labels/1
  # PATCH/PUT /labels/1.json
  def update
    respond_to do |format|
      if @label.update(label_params)
        flash[:success] = "Label was successfully updated."
        format.html { redirect_to @label }
        format.js { }
      else
        flash[:errors] =  @label.errors.full_messages.join("<br>")
        format.html { render :edit }
        format.js { }
      end
    end
  end

  # DELETE /labels/1
  # DELETE /labels/1.json
  def destroy
    @label.destroy
    respond_to do |format|
      flash[:success] = "Label deleted."
      # format.html { redirect_to labels_url, notice: 'Label was successfully destroyed.' }
      format.js {  }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_label
      @label = Label.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def label_params
      params.fetch(:label, {}).permit(:name, :color)
    end
end
