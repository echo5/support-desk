class Admin::ChannelItemsController < ApplicationController
  include RequireTenant
  layout 'admin'
  load_and_authorize_resource
  before_action :set_channel_item, only: [:update, :create_ticket]

  # GET /channel_items
  # GET /channel_items.json
  def index
    @products = Product.all
    @channel_items = ChannelItem.includes(:ticket, :product).filter(filtering_params(params)).paginate(:page => params[:page])
  end

  def sync
    @products = Product.all
    if current_user.envato
      sync_envato
    end
    remove_old
    @channel_items = ChannelItem.includes(:ticket, :item).filter(filtering_params(params)).paginate(:page => params[:page])
    @channel_items.first.touch
    render :layout => false
  end

  # GET /channel_items/sync_envato
  def sync_envato
    @products.each do |item|

      next if item.external_id.nil? || item.source != 'envato'

      json = Envato::API.get('v1/discovery/search/search/comment?page=1&page_size=20&sort_by=newest&item_id=' + item.external_id.to_s, current_user.envato.access_token)  
      comments = json['matches']
      last_imported = ChannelItem.where(item_id: item.id).order('created_at').last
      if last_imported.nil? || last_imported.last_reply_created_at < comments.first['last_comment_at']
        comments.each do |comment|
          conversation = comment['conversation'].first
            ChannelItem.create_with(
              username: conversation['username'],
              content: conversation['content'],
              avatar_url: conversation['profile_image_url'],
              item_id: item.id,
              last_reply_created_at: comment['last_comment_at']
            ).find_or_create_by(
              external_id: comment['id'],
              source: 'envato_comment'
            )
        end
      else
        # No new items found
      end
    end
  end

  def remove_old
    ChannelItem.order('last_reply_created_at desc').offset(20).destroy_all
  end

  def create_ticket

    @identity = Identity.find_or_create_by(provider: 'envato', uid: @channel_item.username)
    if @identity.user
      @user = @identity.user
    else
      password = SecureRandom.hex(8)      
      @identity.build_user(username: @channel_item.username + @channel_item.external_id.to_s, password: password, password_confirmation: password)
      @identity.save(context: :channel)
    end

    @channel_item.build_ticket(
      subject: "Envato comment ##{@channel_item.external_id}",
      user_id: @identity.user.id,
      item_id: @channel_item.item_id,
      content: @channel_item.content
    )
    if @channel_item.save && !@identity.errors.any? && !@identity.user.errors.any?
      redirect_to @channel_item.ticket
    else
      flash[:error] = [
        @identity.errors.full_messages.join('<br>'),
        @channel_item.errors.full_messages.join('<br>'),
        # @identity.user.errors.full_messages.join('<br>'),
      ].join('<br>')
      redirect_to channel_items_url
    end
  end

  # PATCH/PUT /channel_items/1
  # PATCH/PUT /channel_items/1.json
  def update
    respond_to do |format|
      if @channel_item.update(channel_item_params)
        format.html { redirect_to @channel_item, notice: 'Channel item was successfully updated.' }
        format.json { render :show, status: :ok, location: @channel_item }
      else
        format.html { render :edit }
        format.json { render json: @channel_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /channel_items/1
  # DELETE /channel_items/1.json
  def destroy
    @channel_item.destroy
    respond_to do |format|
      format.html { redirect_to channel_items_url, notice: 'Channel item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_channel_item
      @channel_item = ChannelItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def channel_item_params
      params.fetch(:channel_item, {})
    end

    # A list of the param names that can be used for filtering
    def filtering_params(params)
      params.slice(:item)
    end
end
