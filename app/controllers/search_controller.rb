class SearchController < ApplicationController
  include RequireTenant
  before_action :set_options, only: :index
  before_action :set_models, only: :index
  before_action :set_items, only: :index


  # GET /search
  # GET /search.json
  def index
    if params[:q].nil?
      @results = []
    else
      query = params[:q].gsub(/ /, ' | ')
      @results = ThinkingSphinx.search(query, @options)
    end
  end

  private

    def set_options
      @options = {
        :match_mode => :any,
        :star => true,
        :per_page => 10,
        :field_weights => {
          :title => 10,
          :subject => 5,
        }
      }
    end

    def set_items

      if !current_user.is_agent?
        if @settings.articles_private == true
          item_ids = current_user.purchases.pluck(:item_id)
        end
      end

      if params[:item_id]
        # Prevent spoofing
        if item_ids
          item_ids = ([params[:item_id].to_i]).to_a & item_ids
        else
          item_ids = params[:item_id].to_i
        end
      end

      if item_ids
        @options.merge!(:with => {:item_id => item_ids})
      end

    end


    def set_models
      if current_user.is_agent?
        classes = [Article, Ticket]
      else
        classes = [Article]
        if @settings.tickets_private == false
          classes.push(Ticket)
        end
      end
      @options.merge!(classes: classes)
    end

end
