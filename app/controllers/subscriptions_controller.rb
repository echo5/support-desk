class SubscriptionsController < ApplicationController
  include Payola::StatusBehavior
  layout :tenant_or_frontend

  before_action :find_plan_coupon_quantity_and_trial_end, only: [:create, :change_plan]
  before_action :confirm_max_agents, only: [:create, :change_plan]
  before_filter :set_subscription, only: [:show]
  before_filter :load_plans, only: [:show, :index]

  def create
    owner = current_tenant
    params[:plan] = @plan
    subscription = Payola::CreateSubscription.call(params, owner)
    flash[:success] = 'You\'ve successfully subscribed!'
    render_payola_status(subscription)
  end

  def cancel
    subscription = current_tenant.subscription
    Payola::CancelSubscription.call(subscription, at_period_end: true)
    flash[:success] = 'Your subscription has been canceled.'
    redirect_to :back
  end

  def change_plan
    @subscription = current_tenant.subscription
    Payola::ChangeSubscriptionPlan.call(@subscription, @plan, @quantity, @coupon)
    @subscription.cancel_at_period_end = false
    if @subscription.save
      flash[:success] = 'Your subscription plan has been updated.'
      redirect_to :back
    else
      flash[:error] = 'There was a problem updating your subscription.'
    end
  end

  private

    def set_subscription
      if current_tenant.subscription
        @subscription = current_tenant.subscription
      else
        @subscription = current_tenant.build_subscription
      end
    end

    def load_plans
      @subscription_plans = SubscriptionPlan.order(:id)
    end

    def confirm_max_agents
      if @quantity < User.total_agents
        flash[:error] = 'You have too many agents.  Please add more to your plan or remove agents from your account.'
        redirect_to :back
      end
    end

    def find_plan_coupon_quantity_and_trial_end
      find_plan
      find_coupon
      find_trial_end
      find_quantity
    end

    def find_plan
      @plan = SubscriptionPlan.find_by!(id: params[:plan_id])
    end

    def find_coupon
      @coupon = cookies[:cc] || params[:cc] || params[:coupon_code] || params[:coupon]
    end

    def find_trial_end
      @trial_end = params[:trial_end]
    end

    def find_quantity
      @quantity = params[:quantity].blank? ? 1 : params[:quantity].to_i
    end
    
    def tenant_or_frontend
      if current_tenant
        "application"
      else
        "frontend"
      end
    end
end