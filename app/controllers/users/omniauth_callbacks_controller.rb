class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def default
      auth = request.env['omniauth.auth']
      # Find an identity here
      @identity = Identity.find_with_omniauth(auth)

      if @identity.nil?
        # If no identity was found, create a brand new one here
        @identity = Identity.create_with_omniauth(auth)
      elsif @identity.access_token.nil?
        @identity.update_with_omniauth(auth)
      end
    
      if current_user
        if @identity.user && @identity.user.last_sign_in_at.nil? && @identity.user.email.blank?
          # Account has never been accessed and can probably be safely destroyed
          # Most likely happens in the event of importing or channels
          User.merge_accounts(@identity.user,current_user)
          @identity.user = current_user
          @identity.save
          redirect_to root_url, notice: "Successfully linked your account."                    
        elsif @identity.user && @identity.user != current_user
          # Identity associated with another account
          redirect_to root_url, notice: "That account is already linked to another account."          
        elsif @identity.user == current_user
          # User is signed in so they are trying to link an identity with their
          # account. But we found the identity and the user associated with it 
          # is the current user. So the identity is already associated with 
          # this user. So let's display an error message.
          redirect_to root_url, notice: "You have already linked this account."
        else
          # The identity is not associated with the current_user so lets 
          # associate the identity
          @identity.user = current_user
          @identity.save
          redirect_to root_url, notice: "Successfully linked your account."
        end
      else
        if @identity.user.present?
          # The identity we found had a user associated with it so let's 
          # just log them in here
          sign_in_and_redirect @identity.user, :event => :authentication #this will throw if @user is not activated
        else
          @user = User.find_or_create_with_omniauth(auth)
          if @user.errors.any?
            # No user associated with the identity so we need to create a new one
            flash[:error] = @user.errors.full_messages.join('<br>')
            redirect_to new_user_registration_url #, notice: "Please finish registering"
          else
            # Associate identity by user email
            @identity.user = @user
            @identity.save
            sign_in_and_redirect @identity.user, :event => :authentication #this will throw if @user is not activated          
          end
        end
      end

    # @user = User.from_omniauth(request.env["omniauth.auth"])

    # if @user.errors.any?
    #   flash[:error] = @user.errors.full_messages.join('<br>')
    #   redirect_to new_user_registration_url        
    # elsif @user.persisted?
    #   sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated
    #   set_flash_message(:notice, :success, :kind => "Facebook") if is_navigational_format?
    # else
    #   session["devise.facebook_data"] = request.env["omniauth.auth"]
    #   redirect_to new_user_registration_url
    # end
  end

  def failure
    flash[:error] = 'Login failed.'
    redirect_to root_path
  end

  alias_method :facebook, :default
  alias_method :envato, :default

end