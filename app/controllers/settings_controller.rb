class SettingsController < ApplicationController
	include RequireTenant
	load_and_authorize_resource
	# before_action :get_setting, only: [:update]
	before_action :collection
	before_action :validate_settings, only: :update_all

	# GET /settings
	def index
		@labels = Label.all
		@label = Label.new
		@canned_responses = CannedResponse.all
		@canned_response = CannedResponse.new
		@admins = User.where('role', :admin)
		@assignee_options = User.where(role: 'admin')
	end

	# PATCH /settings/update_all
	def update_all
		if params[:settings][:branding_logo]
			@setting = Setting.find_by(var: 'branding_logo' || Setting.new(var: 'branding_logo'))
      @asset = @setting.create_asset(attachment: params[:settings][:branding_logo])
      @setting.value = @asset.id
      @setting.save
      if @asset.errors.any?
      	@errors[:branding_logo] = @asset.errors['attachment']
    	end
		end
		if @errors.any?
      flash[:error] = @errors.full_messages.join("<br>") 
		  # flash[:error] = @errors
		else
		  coerced_values.each { |name, value| RailsSettingsUi.settings_klass[name] = value }
		  flash[:success] = t('settings.index.settings_saved')
		end
		respond_to do |format|
	    format.html { redirect_to settings_path }
	    format.js { }
	  end
	end

	private

		def collection
		  all_settings = default_settings.merge(RailsSettingsUi.settings_klass.public_send(:get_all))
		  all_settings_without_ignored = all_settings.reject{ |name, description| RailsSettingsUi.ignored_settings.include?(name.to_sym) }
		  @settings = Hash[all_settings_without_ignored]
		  @errors = {}
		end

		def settings_from_params
		  settings_params = params['settings'].deep_dup
		  if settings_params.respond_to?(:to_unsafe_h)
		    settings_params.to_unsafe_h
		  else
		    settings_params
		  end
		end

		def validate_settings
		  # validation schema accepts hash (http://dry-rb.org/gems/dry-validation/forms/) so we're converting
		  # ActionController::Parameters => ActiveSupport::HashWithIndifferentAccess
		  @errors = RailsSettingsUi::SettingsFormValidator.new(default_settings, settings_from_params).errors
		end

		def coerced_values
		  RailsSettingsUi::SettingsFormCoercible.new(default_settings, settings_from_params).coerce!
		end

		def default_settings
		  RailsSettingsUi.default_settings
		end

end
