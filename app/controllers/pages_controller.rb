class PagesController < ApplicationController
  include NoTenant
  layout 'landing'
  load_and_authorize_resource
  before_action :set_page, only: [:show, :edit, :update]

  # GET /pages/1
  # GET /pages/1.json
  def show
  end

  # GET /pages/1/edit
  def edit
  end

  # PATCH/PUT /pages/1
  # PATCH/PUT /pages/1.json
  def update
    respond_to do |format|
      if @page.update(page_params)
        format.html { redirect_to @page, notice: 'Page was successfully updated.' }
        format.json { render :show, status: :ok, location: @page }
      else
        format.html { render :edit }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_page
      @page = Page.find_by_slug!(params[:slug])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def page_params
      params.fetch(:page, {})
    end
end
