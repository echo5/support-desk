class TicketsController < ApplicationController
  include RequireTenant
  before_action :set_ticket, only: [:edit, :update, :update_status, :destroy, :assign]
  before_action :get_agents, only: [:show]
  before_action :get_labels
  before_action :get_products, only: [:new, :create]
  before_action :validate_product, only: [:create]
  after_action except: [:index] { authorize @ticket }
  after_action only:   [:index] { authorize @tickets }

  # GET /tickets
  # GET /tickets.json
  def index
    if @settings.tickets_private && !current_user
      redirect_to new_user_session_path
    end
    @ticket = Ticket.new
    @tickets = policy_scope(Ticket.includes(:labels, :user).filter(filtering_params(params)).order("tickets.last_reply_created_at desc").paginate(:page => params[:page]))
    authorize @tickets
    # @tickets = @tickets.includes(:labels, :user).filter(filtering_params(params)).order("tickets.last_reply_created_at desc").paginate(:page => params[:page])
  end

  # GET /tickets/1
  # GET /tickets/1.json
  def show
    @canned_responses = CannedResponse.all
    @ticket = Ticket.includes(:product, :labels, :assignee, :attachments, :user, replies: [:user, :attachments]).find(params[:id])
    @reply = Reply.new
  end

  # GET /tickets/new
  def new
    redirect_to new_user_session_path if current_user.nil?
    @ticket = Ticket.new
    @ticket.replies.build
    @ticket.attachments.build
  end

  # GET /tickets/1/edit
  def edit
  end

  # GET /tickets/1/follow
  def follow
    current_user.follow(@ticket)
    flash[:success] = 'You are now following this ticket.'
    redirect_to @ticket
  end

  # GET /tickets/1/unfollow
  def unfollow
    current_user.stop_following(@ticket)
    flash[:success] = 'You will no longer receive notifications on this ticket.'
    redirect_to @ticket
  end

  # POST /tickets
  # POST /tickets.json
  def create
    @ticket = Ticket.new(ticket_params)
    @ticket.status = 0
    @ticket.user_id = current_user.id
    @ticket.replies.first.user_id = current_user.id

    # Auto assignee
    if @settings.tickets_auto_assignee_id
      @ticket.assignee_id = @settings.tickets_auto_assignee_id
    end

    # Add images
    if params[:ticket][:attachments]
      params[:ticket][:attachments].each do |attachment|
        @asset = @ticket.attachments.new(attachment: attachment)
        @ticket.attachments << @asset
      end
    end

    if @ticket.save
      current_user.follow(@ticket)
      redirect_to @ticket, notice: 'Ticket was successfully created.'
    else
      render :new
    end
  end

  def update_status
    # @TODO create reply with status change
    # @TODO Send email about update
    @reply = Reply.new
    if @ticket.update(status: params["status"])
      flash.now[:success] = 'Ticket status updated.'
    else
      flash.now[:error] = 'Error updating ticket status.'
    end
  end

  # PATCH/PUT /tickets/1/assign
  def assign
    if params[:assignee_id].to_i != @ticket.assignee_id
      @ticket.assignee_id = params[:assignee_id]
      @ticket.save
      if !@ticket.assignee_id.nil?
        user = User.find(@ticket.assignee_id)
        TicketMailer.ticket_assigned(user, @ticket).deliver_later
      end
      redirect_to @ticket
    end
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    @ticket.destroy
    respond_to do |format|
      format.html { redirect_to tickets_url, notice: 'Ticket was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def get_labels
      @labels = Label.all
    end

    def get_products
      @products = Item.accessible_by(current_ability, :read) 
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.includes(:assignee).find(params[:id])
    end

    # Get users available to assign tickets to
    def get_agents
      @agents = User.agents
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ticket_params
      if current_user.is_agent?
        params.require(:ticket).permit(:subject, :user_id, :assignee_id, :needs_feedback, :product_id, :status, :priority, :label_ids => [], :replies_attributes => [:content])
      else
        params.require(:ticket).permit(:subject, :product_id, :replies_attributes => [:content])
      end
    end

    def validate_product
      valid_product_ids = @products.map(&:id)
      params[:ticket].delete :product_id unless valid_product_ids.include?(params[:ticket][:product_id].to_i)
    end

    # A list of the param names that can be used for filtering
    def filtering_params(params)
      params.slice(:status, :assignee, :product, :label)
    end

end
