class RepliesController < ApplicationController
  include RequireTenant
  load_and_authorize_resource :ticket
  load_and_authorize_resource :reply, :through => :ticket, :shallow => true
  before_action :set_ticket, only: [:create]
  before_action :set_reply, only: [:edit, :update, :destroy]

  # GET /replies/1/edit
  def edit
  end

  # POST /replies
  # POST /replies.json
  def create
    @ticket = Ticket.find(params[:ticket_id])
    @reply = @ticket.replies.build(reply_params)
    @reply.user_id = current_user.id

    # Add images
    # if params[:reply][:attachments]
    #   params[:reply][:attachments].each do |attachment|
    #     @asset = @reply.assets.new(attachment: attachment)
    #     @reply.assets << @asset
    #   end
    # end

    # # Add dropzone images
    # if params[:attachment_ids]
    #   params[:attachment_ids].each do |attachment_id|
    #     @attachment = Medium.find(attachment_id)
    #     # @TODO more secure way of attaching files
    #     @reply.attachments << @attachment if @attachment.created_at > 30.minutes.ago && @attachment.attachable_id == nil
    #   end
    # end

    respond_to do |format|
      if @reply.save
        @ticket.set_auto_close(@settings, current_user)
        format.html { redirect_to :back, notice: 'Reply was successfully created.' }
      else
        format.html { 
          flash[:error] = @reply.errors.full_messages.join('<br>')
          redirect_to @ticket
        }
      end
    end
  end

  # PATCH/PUT /replies/1
  # PATCH/PUT /replies/1.json
  def update
    respond_to do |format|
      if @reply.update(reply_params)
        format.html { redirect_to @reply.ticket, notice: 'Reply was successfully updated.' }
        format.json { render :show, status: :ok, location: @reply }
      else
        format.html { render :edit }
        format.json { render json: @reply.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /replies/1
  def destroy
    @reply.destroy
    flash[:success] = 'Reply was successfully deleted.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reply
      @reply = Reply.find(params[:id])
    end

    def set_ticket
      @ticket = Ticket.find(params[:ticket_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reply_params
      params.require(:reply).permit(:content, :ticket_id)
    end


end
