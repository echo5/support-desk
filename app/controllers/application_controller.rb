class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery with: :exception
  before_filter :find_current_tenant
  # before_filter :get_account_settings
  set_current_tenant_through_filter
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_filter :set_mailer_host
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  # after_action :verify_authorized
  layout :layout_by_resource

  def find_current_tenant

    # Check if subdomain or cname
    subdomain = request.subdomains[0]
    if ![ENV['APP_DOMAIN'], 'localhost:3000'].include?(request.domain)
      current_account = AccountSetting.find_by!(branding_domain: request.host).account
    elsif subdomain && subdomain != 'www'
      current_account = Account.find_by_subdomain!(subdomain)
    end

    # Set account settings if found
    if current_account
      set_current_tenant(current_account)
      get_account_settings
      get_account_subscription
    else
      set_current_tenant(nil)
    	# redirect_to root_path
    end

  end

  def get_account_settings
    @settings = current_tenant.settings
  end

  def get_account_subscription
    current_tenant.subscription = current_tenant.subscription
  end

  def current_ability
    @current_ability ||= Ability.new(current_user, current_tenant)
  end

  def payola_can_modify_subscription?(subscription)
    subscription.owner.owner_id == current_user.id
  end

  def default_url_options
    if current_tenant
      { host: current_tenant.url }
    else
      { host: ENV['APP_HOST'] }
    end
  end

  def set_mailer_host
    if current_tenant
      ActionMailer::Base.default_url_options[:host] = current_tenant.url
    end
  end

  private

    def layout_by_resource
      if !current_tenant
        'landing'
      elsif current_user && current_user.is_agent?
        'admin'
      else
        'application'
      end
    end

    def user_not_authorized
      flash[:alert] = "You are not authorized to perform this action."
      # redirect_to(request.referrer || new_user_session_path)
    end

  protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :email])
    end

end
