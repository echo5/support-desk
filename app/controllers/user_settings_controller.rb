class UserSettingsController < ApplicationController
  include RequireTenant
  load_and_authorize_resource
  before_action :set_user, only: [:update, :create]

  # POST /user_settings
  # POST /user_settings.json
  def create
    self.update
  end

  # PATCH/PUT /user_settings/1
  # PATCH/PUT /user_settings/1.json
  def update
    @user_settings = UserSetting.find_or_initialize_by(user_id: @user.id)
    respond_to do |format|
      if @user_settings.update(user_setting_params)
        flash[:success] = "User settings were successfully saved." 
        format.html { redirect_to @user }
      else
        flash[:error] = "There was an error updating user settings." 
        format.html { redirect_to @user }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:user_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_setting_params
      params.require(:user_setting).permit(:receive_emails, :follow_on_reply, :follow_on_assign, :follow_all)
    end
end
