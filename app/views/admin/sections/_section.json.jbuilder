json.extract! section, :id, :name, :sort_order, :item_id, :created_at, :updated_at
json.url section_url(section, format: :json)