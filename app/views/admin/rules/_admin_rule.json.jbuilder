json.extract! admin_rule, :id, :created_at, :updated_at
json.url admin_rule_url(admin_rule, format: :json)
