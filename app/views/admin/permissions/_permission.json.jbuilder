json.extract! permission, :id, :scheme_id, :action, :type, :value, :created_at, :updated_at
json.url permission_url(permission, format: :json)
