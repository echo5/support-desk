json.extract! permission_scheme, :id, :name, :created_at, :updated_at
json.url permission_scheme_url(permission_scheme, format: :json)
