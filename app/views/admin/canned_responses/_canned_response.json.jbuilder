json.extract! canned_response, :id, :name, :content, :created_at, :updated_at
json.url canned_response_url(canned_response, format: :json)