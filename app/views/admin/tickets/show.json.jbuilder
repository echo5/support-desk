json.replies(@ticket.replies) do |reply|
  json.merge! reply.as_json
  # json.user reply.user
  # json.url message_url(message, format: :json)
end

json.form do
  json.action admin_ticket_replies_path(@ticket.id)
end