class TicketMailer < ApplicationMailer
	helper TicketMailerHelper

	def new_reply(users, reply)
	  @reply = reply
	  @ticket = Ticket.where(id: reply.ticket_id).first
	  emails = get_emails(users)
	  mail(bcc: emails, subject: 'New Reply')
	end

	def new_ticket(users, ticket)
		@ticket = ticket
	  emails = get_emails(users)
  	mail(bcc: emails, subject: 'New Ticket')
	end

	def ticket_status_changed(users, ticket)
		@ticket = ticket
	  emails = get_emails(users)
  	mail(bcc: emails, subject: 'Ticket Status Updated')
	end

	def ticket_assigned(user, ticket)
	  @ticket = ticket
	  mail(to: user.email, subject: 'Ticket Assigned')
	end

end
