class ApplicationMailer < ActionMailer::Base
  default from: 'info@deskimo.io'
  layout 'mailer'
end
