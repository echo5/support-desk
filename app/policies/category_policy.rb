class CategoryPolicy < ApplicationPolicy
  attr_reader :user, :category

  class Scope < Scope
    attr_reader :user, :category
      
    def initialize(user, category)
      @user = user || User.new
      @category = category
    end

    def resolve

      items_with_rules_ids = []
      granted_ids = []
      # @rule_sets = RuleSet.includes(:rules,:items).references(:rules).where("rules.action = 'read'") .where("rules.model = 'Article'")
      # @rule_sets.each do |rule_set|
        # @items = Category.by_rule_set(rule_set, user)
      #end



      # category = @categories
      category.all if user.is_agent? || @rule_sets.empty?
    end
  end

  def initialize(user, category)
    @user = user || User.new
    @category = category
  end

  def index?
    true
  end

  def create?
    user.is_agent?
  end

  def show?
    user.is_agent? || category.is_published
  end

  def destroy?
    user.is_agent?
  end

end
