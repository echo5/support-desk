class TicketPolicy < ApplicationPolicy
  attr_reader :user, :ticket

  class Scope < Scope
    attr_reader :user, :ticket
      
    def initialize(user, ticket)
      @user = user || User.new
      @ticket = ticket
    end

    def resolve
      ticket.all #if user.is_agent?
    end
  end

  def initialize(user, ticket)
    @user = user || User.new
    @ticket = ticket
  end

  def index?
    true
  end

  def create?
    user.is_agent?
  end

  def show?
    return true
    user.is_agent?
  end

  def destroy?
    user.is_agent?
  end

end
