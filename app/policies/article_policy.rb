class ArticlePolicy < ApplicationPolicy
  attr_reader :user, :article

  class Scope < Scope
    attr_reader :user, :article
      
    def initialize(user, article)
      @user = user || User.new
      @article = article
    end

    def resolve
      article.all #if user.is_agent?
    end
  end

  def initialize(user, article)
    @user = user || User.new
    @article = article
  end

  def index?
    true
  end

  def create?
    user.is_agent?
  end

  def show?
    return true
    user.is_agent?
  end

  def destroy?
    user.is_agent?
  end

end
