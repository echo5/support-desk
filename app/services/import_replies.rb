class ImportReplies 
  include Import

  attr_reader :errors

  def initialize(params)
    @file ||= params[:file]
    @url ||= params[:url]
    @errors = []
  end

  def call    
    if @url
      import_from_url(@url)
    elsif @file
      import_from_file(@file)
    end
  end


  private

    def format_params(data)
      begin
        defaults = {}
        params = ActionController::Parameters.new(reply: data)
        reply_params = params.require(:reply).permit(:external_id, :content, :ticket_id, :user_id, :created_at)
        reply_params = defaults.merge(reply_params)
        reply = Reply.new(reply_params)
        return reply
      rescue => ex
        errors << ex.message
      end
    end

  # Run batch of imports
    def import_objects(objects)
      begin
        import = Reply.ar_import objects, { validate_with_context: :import }
      rescue => ex
        errors << ex.message
      end
      unless errors.any?
        self.inserted_ids = import.ids
        associate_imports
      end
    end

    # Update user IDs and ticket IDs on imported replies
    def associate_imports
      associated_replies = Reply.connection.update("UPDATE replies SET user_id = users.id, ticket_id = tickets.id FROM users, tickets WHERE replies.user_id = users.external_id AND replies.ticket_id = tickets.external_id AND replies.id IN (#{inserted_ids.join(',')})")
    end

end

