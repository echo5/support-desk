class ImportUsers
  include Import

  attr_reader :errors

  private

    def format_params(data)
      begin
        defaults = {role: 'customer' }
        if !data['password'] || data['password'].length < 6
          temp_password = Devise.friendly_token[0,20]
          data['password'] = data['password_confirmation'] = temp_password
        end

        params = ActionController::Parameters.new(user: data)
        user_params = params.require(:user).permit(:external_id, :name, :username, :email, :created_at, :password, :password_confirmation)
        user_params = defaults.merge(user_params)
        user = User.new(user_params)
        return user
      rescue => ex
        errors << ex.message
      end
    end

  # Run batch of imports
    def import_objects(objects)
      begin
        import = User.import! objects, { validate_with_context: :import }
      rescue => ex
        errors << ex.message
      end
      unless errors.any?
        self.inserted_ids = import.ids
      end
    end

end

