class ImportTickets 
  include Import

  attr_reader :errors

  def initialize(params)
    @file ||= params[:file]
    @url ||= params[:url]
    @errors = []
  end

  def call    
    if @url
      import_from_url(@url)
    elsif @file
      import_from_file(@file)
    end
  end


  private

    def format_params(data)
      begin
        defaults = {status: 'closed', needs_feedback: 0 }
        params = ActionController::Parameters.new(ticket: data)
        ticket_params = params.require(:ticket).permit(:external_id, :subject, :content, :user_id, :assignee_id, :needs_feedback, :item_id, :status, :priority, :created_at, :last_reply_created_at, :label_ids => [])
        ticket_params = defaults.merge(ticket_params)
        ticket = Ticket.new(ticket_params)

        # Build replies if they exist (JSON only)
        if data['replies'] && data['replies'].count
          ticket.replies.build(data['replies'])
        end
        return ticket
      rescue => ex
        errors << ex.message
      end
    end

  # Run batch of imports
    def import_objects(objects)
      begin
        import = Ticket.ar_import objects, { recursive: true, validate_with_context: :import }
      rescue => ex
        errors << ex.message
      end
      unless errors.any?
        self.inserted_ids = import.ids
        associate_imports
      end
    end

    # Update user IDs on imported tickets
    def associate_imports
      associated_tickets = Ticket.connection.update("UPDATE tickets SET user_id = users.id FROM users WHERE tickets.user_id = users.external_id AND tickets.id IN (#{inserted_ids.join(',')})")
      # Import elastic search index
      Ticket.bulk_import
    end
end

