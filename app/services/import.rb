require 'csv'

module Import
	attr_reader :errors

	def initialize(params)
	  @file ||= params[:file]
	  @url ||= params[:url]
	  @errors = []
	end

	def call    
	  if @url
	    import_from_url(@url)
	  elsif @file
	    import_from_file(@file)
	  end
	end


	# private

	  attr_accessor :inserted_ids

	  def parse_csv(file)
	    objects = []
	    begin
	      CSV.foreach(file.path, headers: true) do |row|
	        objects << format_params(row.to_h)
	      end
	    rescue => ex
	      abort ex.message
	    end
	    # options = {:chunk_size => 100, :key_mapping => {:unwanted_row => nil, :old_row_name => :new_name}}
	    # n = SmarterCSV.process(filename, options) do |chunk|
	    #       # we're passing a block in, to process each resulting hash / row (block takes array of hashes)
	    #       # when chunking is enabled, there are up to :chunk_size hashes in each chunk
	    #       MyModel.collection.insert( chunk )   # insert up to 100 records at a time
	    # end
	    import_objects(objects)
	  end

	  def parse_json(json)
	    records = JSON.parse(json)
	    objects = []
	    records.each do |record|
	      objects << format_params(record)
	    end
	    self.objects = objects
	    import_objects(objects)
	  end

	  # Determine source type
	  def import_from_file(file)
	    case
	    when file.try(:content_type) == "text/csv"
	      parse_csv(file)
	    when file.try(:content_type) == "application/json"
	      json = File.read(file.path)
	      parse_json(json)
	    else
	      errors << 'The input is not a valid file format.  Please use JSON or CSV format for uploading.'
	    end

	  end

	  def import_from_url
	    # @TODO add yagl JSON streamer
	  end

	  # def valid_json?(json)
	  #   JSON.parse(json)
	  #     return true
	  #   rescue JSON::ParserError => e
	  #     return false
	  # end


end