class Label < ApplicationRecord
	acts_as_tenant(:account)
	has_and_belongs_to_many :tickets
	validates_presence_of :name, :color


	def circle
		"<span class=\"label-circle\" style=\"background-color:#{self.color}\"></span>"
	end

end
