class Product < ApplicationRecord
  acts_as_tenant(:account)  
  has_attached_file :image
	validates_attachment :image, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
	validates_presence_of :name, :slug
  resourcify
  has_many :users, through: :roles, class_name: 'User', source: :users
# has_many :users, -> (product) { includes(:roles_for_product) }, through: :roles, class_name: 'User', source: :users
# has_many :users_with_roles, -> (product) { users.with_roles_for_product(self.id).distinct(user.id) }
  def to_param
    "#{self.slug}"
  end
end
