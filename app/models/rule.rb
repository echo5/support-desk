class Rule < ApplicationRecord
  belongs_to :rule_set
  attr_accessor :user, :model_action

  # validates :model, :presence => true
  validates :action, :presence => true
  validates :operator, :inclusion => { in: :allowed_operators }
  validates :model_key, :inclusion => { in: :allowed_model_keys }
  # before_validation :split_model_action

  def user_value
    value = user.send(user_key)
    value.join(',') if value.kind_of?(Array)
    value
  end

  ACTIONS = {
    'Create Ticket' => 'ticket.create',
    'View Ticket' => 'ticket.read',
    'View Item' => 'item.read',
  }

  # OPERATORS = {
  #   'Equal to' => '==',
  #   'Not equal to' => '!=',
  #   'Greater than' => '>',
  #   'Greater than or equal to' => '>=',
  #   'Less than' => '<',
  #   'Les than or equal to' => '<=',
  #   'Includes' => '.includes?',
  # }

  OPERATORS = [
 '==',
 '!=',
 '>',
 '>=',
 '<',
 '<=',
 '.includes?'
  ]

  USER_KEYS = User.allowed_rule_keys

  # MODEL_KEYS = ['user_condition'] + Item.allowed_rule_keys + Ticket.allowed_rule_keys

  def allowed_operators
    [
      '==',
      '!='
    ]
  end

  def allowed_model_keys
    allowed = []
    # case self.action
    #   when 'ticket.create', 'item.read'
    #     allowed << Item.allowed_rule_keys
    #   when 'ticket.read'
    #     allowed << Ticket.allowed_rule_keys
    # end
    allowed
  end

  # private
  #   def split_model_action
  #     if self.model_action
  #       model_action = self.model_action.split("_", 2) 
  #       self.model = model_action.first
  #       self.action = model_action.second
  #     end
  #   end


end
