class Role < ApplicationRecord
  scopify  
  acts_as_tenant(:account)
  has_many :users_roles, :dependent => :delete_all, class_name: 'UsersRole'
  has_many :users, :join_table => :users_roles
  validates :name, presence: true, uniqueness: { scope: [:resource_id, :account_id] }
  
  belongs_to :resource,
             :polymorphic => true,
             :optional => true

  validates :resource_type,
            :inclusion => { :in => Rolify.resource_types },
            :allow_nil => true

  validate :global_role

  def global_role
    if self.resource_id && !Role.available.include?(self.name)
      errors.add(:base, "this role is not available.")
    end
  end
            
  def self.available
    where(resource_id: nil)
  end

end
