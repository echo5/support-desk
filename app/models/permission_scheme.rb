class PermissionScheme < ApplicationRecord
  has_many :permissions, foreign_key: :scheme_id, inverse_of: :scheme
  has_many :actions, -> { group(:action) }, foreign_key: :scheme_id, inverse_of: :scheme, class_name: 'Permission'
  accepts_nested_attributes_for :permissions
  accepts_nested_attributes_for :actions

end
