class EnvatoAPI

	# Pass any URL and access_token
	def self.get(url, access_token = nil)
		uri = URI.parse('https://api.envato.com/' + url)
		request = Net::HTTP::Get.new(uri)
		request['Authorization'] = 'Bearer ' + access_token
		req_options = {
		  use_ssl: uri.scheme == 'https',
		}
		response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
		  http.request(request)
		end
		self.parse_response(response)
	end

	# Initial token on app approval
	def self.get_access_token(code)
		uri = URI.parse('https://api.envato.com/token')
		params = {
			'grant_type' => 'authorization_code',
			'code' => code,
			'client_id' => ENV['ENVATO_CLIENT_ID'],
			'client_secret' => ENV['ENVATO_API_KEY']
		}
		response = Net::HTTP.post_form(uri, params)
		self.parse_response(response)
	end

	# Refresh token after expiration
	def self.refresh_access_token(refresh_token)
		uri = URI.parse('https://api.envato.com/token')
		params = {
			'grant_type' => 'refresh_token',
			'refresh_token' => refresh_token,
			'client_id' => ENV['ENVATO_CLIENT_ID'],
			'client_secret' => ENV['ENVATO_API_KEY']
		}
		response = Net::HTTP.post_form(uri, params)
		self.parse_response(response)
	end

	def self.parse_response(response)
		JSON.parse(response.body)
	end

end
