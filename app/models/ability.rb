class Ability
  include CanCan::Ability

  def initialize(user, tenant)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
      # @settings = AccountSetting.find(1) || Setting.new


      tenant ||= Account.new
      @settings = tenant.settings
      @subscription = tenant.subscription

      user ||= User.new # guest user (not logged in)

      # Frontend
      can [:read], Page, is_published: true
      can [:read], Post, is_published: true

      # Helpdesk
      case user.role when "agent","owner" then
        can :manage, :all
      else
        valid_item_ids = []
        if user.persisted? && user.envato_purchases
            valid_item_ids = user.envato_purchases.map{|purchase| 
                purchase['item_id'] unless purchase['supported_until'] < Time.now
            }
        end

        # Account settings

        # Knowledgebase
        can [:read], Section
        # Tickets
        can [:unfollow], Ticket
        if @settings.tickets_private
            can [:read], Ticket, user_id: user.id
        else
            can [:read, :follow], Ticket
        end
        if @settings.tickets_require_valid_support
            can [:create], Ticket
            # can [:create], Ticket, item_id: valid_item_ids
            # abort valid_purchases.inspect
            can [:create], Reply, ticket: { item_id: valid_item_ids }
            # can [:edit], Reply, user_id: user.id, ticket: { item_id: valid_item_ids }
        else
            can [:create], Ticket
            can [:create], Reply
            # can [:edit], Reply, user_id: user.id
        end

        # Users
        can [:sync_envato_purchases], User
        can [:show, :update], User, id: user.id
        can [:show, :update, :create], UserSetting, user_id: user.id
      end
  end
end
