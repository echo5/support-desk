class Ticket < ApplicationRecord
	acts_as_tenant(:account)
	
	include Filterable

	self.per_page = 20

	# Filtering scopes
	scope :status, -> (status) { where status: status }
	scope :assignee, -> (assignee_id) { where assignee_id: assignee_id }
	scope :label, -> (label_id) { joins(:labels).where("labels.id = ? ", label_id) }
	scope :product, -> (product_id) { where product_id: product_id }
	scope :starts_with, -> (name) { where("name like ?", "#{name}%")}

	# Relationships
	belongs_to :user
	belongs_to :external_user, class_name: 'User', :foreign_key => 'user_id', :primary_key => "external_id"
	belongs_to :product
	belongs_to :purchase
	belongs_to :assignee, -> {where role: [:agent, :owner]}, class_name: 'User'
	has_many :replies, dependent: :destroy, inverse_of: :ticket
	has_one :channel_item
	has_and_belongs_to_many :labels
  has_many :attachments, :as => :attachable, :class_name => "Image", :dependent => :destroy
	has_many :jobs, :as => :owner, :class_name => "::Delayed::Job"
	enum status: {
			open: 0,
			pending: 1,
			on_hold: 2,
			solved: 3,
			closed: 4,
	}
	acts_as_followable

	accepts_nested_attributes_for :replies
	accepts_nested_attributes_for :purchase
	accepts_nested_attributes_for :attachments
	attr_accessor :importing

	# Hooks
	before_create :set_last_reply_created_at, unless: :importing
	after_commit :send_email, :on => :create, unless: :importing
	after_save ThinkingSphinx::RealTime.callback_for(:ticket)

	# Validation
	before_validation :set_product
	validate :validate_support
	validates_presence_of :user_id, :subject, :status, :product_id
	validate :has_external_user, unless: -> { validation_context != :import } 

	def has_external_user
		external_users = Rails.cache.fetch("external_users", :expires_in => 1.minutes) do
		  User.where('external_id IS NOT NULL').pluck(:external_id)
		end
		if !external_users.include?(self.user_id)
			errors.add(:base, "No user was found with an external ID of " + self.user_id.to_s)
		end
 	end

 	def search_json
 		{
 			type: 'Ticket',
 			content: self.subject
 		}
	end

	def self.allowed_rule_keys
		self.attribute_names
	end

	def as_indexed_json(options={})
	  self.as_json(
	    only: [:subject, :account_id],
	    include: { product: { only: :id}}
	  )
	end

	def self.bulk_import
	  self.preload(:product).find_in_batches do |indexables|
	    bulk_index(indexables)
	  end
	end
	
	def self.bulk_index(indexables)
	  self.__elasticsearch__.client.bulk({
	    index: self.__elasticsearch__.index_name,
	    type: self.__elasticsearch__.document_type,
	    body: prepare_records(indexables)
	  })
	end
	
	def self.prepare_records(indexables)
	  indexables.map do |indexable|
	    { index: { _id: indexable.id, data: indexable.as_indexed_json } }
	  end
	end

	def set_product
	  unless self.purchase.nil?
	    self.product_id = self.purchase.product_id
	  end
	end

	def validate_support
	  unless self.purchase.nil?
	    if !self.purchase.supported?
		    self.errors.add(:purchase_id, "is no longer supported")
	    end
	  end    
	end

	def send_email
	 	users = User.where(role: User.roles['admin']).to_a
	 	TicketMailer.new_ticket(users, self).deliver_later
	end

	def search_json
		{
			type: 'Ticket',
			content: self.subject
		}
	end

	def set_auto_close(settings, user)
	  if settings.tickets_auto_close
	    if user.is_agent?
	      AutoCloseTicketWorker.perform_in(settings.tickets_auto_close_days.to_i.days, self.id, self.last_reply_created_at)
	    end
	  end
	end

	private

		def set_last_reply_created_at
		  self.last_reply_created_at = Time.now
		end


end