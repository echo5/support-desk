class Article < ApplicationRecord
  acts_as_tenant(:account)

  include Filterable
  belongs_to :section
  has_many :attachments, :as => :attachable, :class_name => "Image", :dependent => :destroy
  enum status: {
      draft: 0,
      published: 1,
  }
  
  # Validation
  validates :title, :section, :items, :content, presence: true

  # Scope
  acts_as_sortable
  default_scope { ordered_by_position_asc }
  scope :with_sections, -> {includes(:section)}
  scope :title, -> (title) { where("title like ?", "%#{title}%")}
  scope :section, -> (section) { where(section_id: section) }
  scope :status, -> (status) { where(status: status) }

  before_validation :replace_inline_images
  before_save :filter_content
  # after_save :replace_asset_images
  after_save ThinkingSphinx::RealTime.callback_for(:article)

  
  def search_json
    {
      type: 'Article',
      content: self.title
    }
  end

  def as_indexed_json(options={})
    self.as_json(
      only: [:title, :content, :account_id],
      include: { items: { only: :id}}
    )
  end

  def to_param
    "#{id}-#{self.title.parameterize}"
  end

  def search_json
  	{
  		type: 'Article',
  		content: self.title
  	}
  end

  private
    def replace_inline_images
      if self.content.present?
        doc = Nokogiri::HTML(self.content)
        doc.css("img").each do |img|
          m=/data\:image\/(\w+)\;base64\,(.+)/.match(img[:src])
          next unless m
          @asset = self.assets.new
          @asset.image_data = m[2]
          @asset.content_type = m[1]
          @asset.original_filename = self.title.parameterize
          self.assets << @asset
          img.set_attribute('class', 'asset')
          img.set_attribute('data-asset', @asset.id)
          img.set_attribute('src', @asset.url)
          # abort @asset.url.inspect
        end
        self.content = doc.at('body').inner_html.gsub("\n", "")
      end
    end

    def filter_content
      self.content = Sanitize.fragment(self.content, Sanitize::Config.merge(Sanitize::Config::RELAXED,
      :attributes => {
        'img' => [:data, 'src']
      },
      :protocols => {
        'a'   => {'href' => ['ftp', 'http', 'https', 'mailto']},
        'img' => {'src'  => ['http', 'https', :relative]}
      }))
    end

    def replace_asset_images
      updated = false
      doc = Nokogiri::HTML(self.content)
      doc.css("img.asset").each do |img|
        asset_id = img.attr('data-asset').to_i
        if asset_id && img[:srcfdsa].trim.empty?
          if @asset
            @asset = Asset.find(asset_id)
            img[:src] = @asset.url
            updated = true
          end
        end
      end
      if updated
        self.content = doc.at('body').inner_html.gsub("\n", "")
        self.save
      end
    end

end