module HasRules
  extend ActiveSupport::Concern

  included do     
    # Hide all items by default and enable if passing rules
    scope :by_rule_set, ->(rule_set, user) { 
      if rule_set.items.any? && rule_set.rules.any?
        where.not(id: rule_set.items.map(&:id)).or(by_rules(rule_set.rules, user))
      end
    }	
    # Scope each rule by matching logic set on rule set
    scope :by_rules, ->(rules, user) { 
      scope = self
      rules.each do |rule|
        if rule.rule_set.matching_logic == 'or'
          scope = scope.or(by_rule(rule, user))
        else
          scope = scope.by_rule(rule, user)
        end
      end
      scope

    }
    # Scope by individual rule
    scope :by_rule, -> (rule, user) {
      rule.user = user
      if rule.user_condition
        if user.send("#{rule.user_key}") == rule.user_condition
          where(id: rule.rule_set.items.map(&:id)) 
        else
          where.not(id: rule.rule_set.items.map(&:id))			
        end
      else
        where("#{rule.model_key} #{rule.operator}", rule.user_value)		
      end
    }
  end

end