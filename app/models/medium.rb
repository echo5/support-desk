class Medium < ApplicationRecord
	acts_as_tenant(:account)
	belongs_to :attachable, :polymorphic => true
  belongs_to :account
	delegate :url, :to => :attachment
	validate :account_limit
	has_attached_file :attachment,
	  :styles => 	  {
	    :thumb => "60x60>",
	    :medium => "800x800>"
	  },
	  :url => "/system/:account_id/:class/:attachment/:id_partition/:style/:filename",
	  :path => ':rails_root/public:url'
  validates_attachment :attachment, presence: true,
    content_type: { content_type: ["image/jpeg", "image/jpg", "image/png", "image/gif", "application/pdf", "application/msword", "application/pdf","application/vnd.openxmlformats-officedocument.wordprocessingml.document"] },
    size: { in: 0..2.megabyte }
	validates_attachment_size :attachment, :less_than => 2.megabyte

	def account_limit
		total_storage = self.account.media.sum(:attachment_file_size)
		allowed_storage = 524288000 #self.account.subscription ? self.account.subscription.quantity : 0
		if total_storage >= allowed_storage
		  errors.add(:base, 'This account has reached the maximum storage capacity.  Please upgrade or remove unused files.')
		end
	end

end
