class Identity < ApplicationRecord
  # Validations
  validates :uid, presence: true, uniqueness: { scope: [:provider, :account_id] }
  # Relationships
  acts_as_tenant(:account)
  belongs_to :user
  accepts_nested_attributes_for :user

  def self.find_with_omniauth(auth)
    find_by(uid: auth['uid'], provider: auth['provider'])
  end

  def self.create_with_omniauth(auth)
    create(
      uid: auth['uid'], 
      provider: auth['provider'],
      access_token: auth['credentials']['token'],
      refresh_token: auth['credentials']['refresh_token'],
      expires_at: Time.at(auth['credentials']['expires_at']),
    )
  end

  def update_with_omniauth(auth)
    update(
      uid: auth['uid'], 
      provider: auth['provider'],
      access_token: auth['credentials']['token'],
      refresh_token: auth['credentials']['refresh_token'],
      expires_at: Time.at(auth['credentials']['expires_at']),
    )
  end

	def get_access_token
    if Time.now > self.expires_at
      # @TODO switch case for refreshing token
			json = Envato::API.refresh_access_token(self.refresh_token)
      if json['error']
        # flash[:error] = json['error_description']
      else        
        self.access_token = json['access_token']
        self.expires_at = Time.now + json['expires_in']
        self.save
      end 
		end
		self.access_token
	end

end
