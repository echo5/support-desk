class UsersRole < ApplicationRecord

  validates :user, presence: true
  validates :role, presence: true

  belongs_to :user, inverse_of: :memberships
  belongs_to :role


  def expired?
    self.expires_at && Time.now > self.expires_at
  end

  def badge
    tooltip = "data-title='Expires #{expires_at}' data-toggle='tooltip' data-placement='top'" if expires_at
    "<span class='badge user-badge role #{'expired' if expired?}' #{tooltip} >#{role.name}</span>".html_safe
  end



end
