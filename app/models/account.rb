class Account < ApplicationRecord
	# Added by Koudoku.
    # has_one :subscription
  has_one :subscription, ->(sub) { where.not(stripe_id: nil) }, class_name: Payola::Subscription, foreign_key: :owner_id
	has_many :media
	has_one :owner, -> { where role: 'owner' }, class_name: 'User', :inverse_of => :account
	has_one :settings, class_name: 'AccountSetting'
	validates :subdomain, :exclusion => { :in => %w(www ftp smtp) }, :presence => true, :uniqueness => true
	accepts_nested_attributes_for :owner

  def trial_days_left
  	days_remaining = (self.created_at - Time.now).to_i  / 1.day + 31
		return days_remaining if days_remaining > 0
		return 0
  	# (self.created_at + 30.days - Time.now).to_i
	end

  def is_trialing?
  	return true if self.trial_days_left > 0
	end

	def has_membership?
		if self.subscription && self.subscription.plan_id != nil && !self.subscription.cancel_at_period_end
			return true
		end
		return false
	end

	def is_expired?
		return true if self.subscription.current_period_end < Time.now
	end

	def url
		return self.settings.branding_domain if self.settings.branding_domain
		return self.subdomain + '.' + ENV['APP_DOMAIN']
	end

	def self.find_by_host(host)
		# Check if subdomain or cname
		# host = URI.parse(url).host.sub(/\Awww\./, '')
		domain = host.split(".")[-2] + '.' + host.split(".")[-1]
		subdomain = host.split(".")[0]
		if [ENV['APP_DOMAIN'], 'localhost:3000'].include?(domain)
			current_account = Account.find_by_subdomain!(subdomain)
		else
			current_account = AccountSetting.find_by!(branding_domain: host).account
		end
	end
	
	def settings_with_default
	  settings_without_default || build_settings
	end
	alias_method_chain :settings, :default
end
