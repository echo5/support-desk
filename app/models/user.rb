class User < ApplicationRecord
  rolify strict: true

  # Relationships
  acts_as_tenant(:account)  
  acts_as_follower  
  has_many :data, dependent: :destroy, class_name: 'UserDatum'
  has_many :identities, dependent: :destroy
  has_many :tickets, dependent: :destroy
  has_many :replies, dependent: :destroy
  has_many :assigned_tickets, class_name: 'Ticket'
  has_one :settings, class_name: 'UserSetting'
  belongs_to :account, :inverse_of => :owner
  accepts_nested_attributes_for :settings
  enum role: [:customer, :agent, :owner]
  accepts_nested_attributes_for :identities  
  # has_attached_file :avatar
  has_many :roles, -> { select('roles.*, users_roles.expires_at as expires_at').joins(:users_roles) }, through: :users_roles
  # has_many :roles, -> { select('roles.*, users_roles.expires_at as expires_at') }, through: :users_roles
  has_many :memberships, :dependent => :delete_all, class_name: 'UsersRole'

  # Scopes
  include Filterable
  scope :role, -> (role) { where role: role }
  scope :username, -> (username) { where("username like ?", "#{username}%")}
  scope :email, -> (email) { where("email like ?", "#{email}%")}
  scope :receive_emails, -> { where('settings LIKE ?', '%"receive_emails":"1"%') }
  scope :agents, -> { where(role: ['agent', 'owner'])}
  self.per_page = 20

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :omniauthable, :omniauth_providers => [:facebook, :envato]
  include Authentication

  # Validation
  # validates_uniqueness_to_tenant :email
  # validates_uniqueness_to_tenant :username
  validates :username, presence: true, length: { minimum: 3, maximum: 64 }, uniqueness: { scope: :account_id }
  validates :email, presence: true, uniqueness: { scope: :account_id }, unless: -> { validation_context == :channel }
  validates :password, presence: true, length: { :in => 6..20 }, :if => :password_required?
  validate :prevent_ownership_role_change
  validate :agent_limit
  validate :username_change

  def as_json(options={})
    super.as_json(options).merge({:display_name => display_name, :avatar => avatar})
  end

  def username_change
    if self.username_changed? && !self.username_change_allowed?
      errors.add(:username, "can only change username within 24 hours of first sign in.")
    end
  end

  def username_change_allowed?
    self.last_sign_in_at.nil? || self.last_sign_in_at > (Time.now - 24.hours) && self.sign_in_count <= 1 && self.username_was != ''
  end

  def envato
    identities.select { |identity| identity.provider == 'envato' }.first
  end

  def envato_purchases
    purchases = data.select { |data| data.key == 'envato_purchases' }
    return purchases.first.info if purchases.first
    return []
  end

  def envato_purchases_ids
    envato_purchases = self.envato_purchases
    return [] if envato_purchases.nil?
    self.envato_purchases.map(&:item_id)
  end

  def envato_supported_purchases_ids
    ids = []
    self.envato_purchases.each do |purchase|
      ids << purchase['item_id'] if purchase['supported_until'] < Time.now
    end
    ids
    # envato_purchases = self.envato_purchases
    # return [] if envato_purchases.nil?
    # self.envato_purchases.select{|purchase| purchase['supported_until'] < Time.now }.map(&:id)
  end

  def self.allowed_rule_keys
		self.attribute_names
	end

  def facebook
    identities.select { |identity| identity.provider == 'facebook' }.first
  end
  

  def agent_limit
    return if !self.account.persisted? # Registering account
    agents_remaining = self.account.subscription ? self.account.subscription.quantity : 0
    if self.role == 'agent' && self.role_changed? && User.total_agents >= agents_remaining
      errors.add(:base, 'You\'ve reached the maximum number of agents available on this account.  Please add more agents to your plan.')
    end
  end

  def prevent_ownership_role_change
    return if !self.account.persisted? # Registering account    
    if self.role_was == 'owner' && self.role_changed?
      errors.add(:role, "can't remove owner from account")
    end
    if self.role == 'owner' && self.role_changed?
      errors.add(:role, "can't add ownership to account")
    end
  end

  def available_roles
    User.roles.except(:owner).map { |key, value|
      [key.titleize, key]
    }
    # if self.role == 'agent'
    #   [:customer, :agent]
    # end
  end
  
  def settings_with_default
    settings_without_default || build_settings
  end
  alias_method_chain :settings, :default

  def badge
  	if self.is_agent?
  	  '<span class="badge user-badge">Staff</span>'.html_safe
  	end
  end

  # def self.from_omniauth(auth)
  #   identity = Identity.first_or_create(provider: auth.provider, uid: auth.uid)
  #   # Already linked
  #   return identity.user if identity.user
  #   # Identity not yet linked
  #   user = User.where(email: auth.info.email).first_or_create do |user|
  #     user.email = auth.info.email
  #     user.channel = 'oauth'
  #     user.password = Devise.friendly_token[0,20]
  #     user.name = auth.info.name   # assuming the user model has a name
  #     # avatar: auth.info.image, # assuming the user model has an image
  #   end
  #   identity.user = user
  #   identity.save
  #   return user
  # end

  def self.find_or_create_with_omniauth(auth)
    where(email: auth.info.email).first_or_create do |user|
      user.email = auth.info.email
      user.channel = 'oauth'
      user.password = Devise.friendly_token[0,20]
      user.name = auth.info.name   # assuming the user model has a name
      # avatar: auth.info.image, # assuming the user model has an image
    end
  end

  def self.build_with_omniauth(auth)
    new(email: auth.info.email)
  end

  def get_envato_items
  	json = Envato::API.get('v1/discovery/search/search/item?username=' + self.envato.uid, self.envato.get_access_token)
  	json['matches']
  end

	def get_envato_purchases
		json = Envato::API.get('v3/market/buyer/list-purchases', self.envato.get_access_token)
		json
	end

  def password_required?
    !persisted? || !password.nil? || !password_confirmation.nil?
  end

  def is_agent?
    case self.role when 'agent', 'owner' then
      return true
    end
    return false
  end

  def display_name
    if !self.name.nil?
      self.name
    else
      self.username
    end
  end

	def avatar(size = 60, classes = '')
		'<img src="' + self.avatar_url(size) + '" alt="' + self.display_name + '" class="avatar ' + classes + '">'
	end

	def avatar_url(size = 60)
    if !self.email.nil?
  		gravatar_id = Digest::MD5.hexdigest(self.email.downcase)
  		"http://gravatar.com/avatar/#{gravatar_id}.png?s=#{size}&d=retro"
    else
      'http://www.gravatar.com/avatar/?d=retro'
    end
	end

  def self.total_agents
    # Include owner +1
    User.where(role: :agent).count + 1
  end

  # Only tested for merging based on channel item tickets
  def self.merge_accounts(old_user, new_user)
    Ticket.where("user_id = ?", old_user.id).update_all(:user_id => new_user.id)
    old_user.destroy
  end

end
