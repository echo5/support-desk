class ChannelItem < ApplicationRecord
	acts_as_tenant(:account)
	belongs_to :ticket
	belongs_to :product
	enum channel: [:envato_comment]
	self.per_page = 20
	accepts_nested_attributes_for :ticket
	include Filterable
	scope :product, -> (product_id) { where product_id: product_id }
	
	# validates_presence_of :ticket, :product, :username

end
