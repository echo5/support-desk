class Section < ApplicationRecord
	acts_as_tenant(:account)

  include Filterable

  has_many :articles
  default_scope { ordered_by_position_asc }
  validates_presence_of :name
  acts_as_sortable
  scope :with_published_articles_for_item, -> (item) {
    includes(:articles).where(articles: {status: 'published', id: item.articles_items.pluck(:article_id)})
  }
  scope :title, -> (title) { where("name like ?", "%#{title}%")}


  def to_param
    "#{id}-#{self.name.parameterize}"
  end
end
