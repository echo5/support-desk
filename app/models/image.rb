class Image < Medium
	has_attached_file :attachment,
	  :styles => 	  {
	    :thumb => "60x60>",
	    :medium => "800x800>"
	  },
	  :url => "/system/:account_id/:class/:attachment/:id_partition/:style/:filename",
	  :path => ':rails_root/public:url'
  validates_attachment :attachment, presence: true,
    content_type: { content_type: ["image/jpeg", "image/jpg", "image/png", "image/gif"] },
    size: { in: 0..700.kilobytes }
	validates_attachment_size :attachment, :less_than => 1.megabyte
	before_validation :decode_base64_image
	attr_accessor :content_type, :original_filename, :image_data

	protected
	  def decode_base64_image
	    if image_data #&& content_type && original_filename
	      decoded_data = Base64.decode64(image_data)

	      data = StringIO.new(decoded_data)
	      # data.class_eval do
	      #   attr_accessor :content_type, :original_filename
	      # end

	      # data.content_type = content_type
	      # data.original_filename = File.basename(original_filename)
	      self.attachment = data
	    end
	  end

end