class RuleSet < ApplicationRecord
  has_many :rules, inverse_of: :rule_set
  has_and_belongs_to_many :items
  accepts_nested_attributes_for :rules, allow_destroy: true
  validates :name, :presence => true  
	enum matching_logic: {
    all: 0,
    any: 1,
  }, _prefix: true
end
