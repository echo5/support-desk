class Category < ApplicationRecord
	include HasRules
	# Relationships
	acts_as_tenant(:account)
	belongs_to :product
	has_many :tickets
	has_many :purchases
	has_many :channel_items
	has_many :users, through: :purchases
	# has_many :articles_items
	has_many :sections
	has_and_belongs_to_many :articles
	has_attached_file :image
	has_and_belongs_to_many :rule_sets
	enum status: {
			draft: 0,
			published: 1,
	}

	# Validation
	validates :name, :slug, presence: true
	validates_attachment :image, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
  validates_uniqueness_to_tenant :slug
  validates_uniqueness_to_tenant :external_id, unless: Proc.new { |item| item.external_id.nil? }

  def to_param
    "#{id}-#{self.name.parameterize}"
  end

	def to_label
	  "#{name}"
	end

	def slug=(value)
	  if value.present?
	    write_attribute(:slug, value.parameterize)
      else
	    write_attribute(:slug, self.name.parameterize)
	  end
	end

	def image_from_url(url)
	  self.image = URI.parse(url)
	end

	def self.allowed_rule_keys
		self.attribute_names
	end

end
