class Post < ApplicationRecord
	enum status: {
			draft: 0,
			published: 1,
	}
	self.per_page = 10
end
