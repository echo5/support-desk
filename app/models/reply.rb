class Reply < ApplicationRecord
	acts_as_tenant(:account)
	belongs_to :ticket, inverse_of: :replies
	belongs_to :user
	validates_presence_of :user_id, :content, :ticket
	before_save :filter_content
	after_save :follow_ticket, :set_ticket_feedback
	after_commit :send_email, :on => :create
  has_many :attachments, :as => :attachable, :class_name => "Medium", :dependent => :destroy
  default_scope { order(created_at: :asc) }

	def send_email
		reply = self
	 	users = reply.ticket.followers.reject{ |user| user.id == reply.user_id }
	 	TicketMailer.new_reply(users, reply).deliver_later
	end

	private
		def filter_content
			self.content = Sanitize.fragment(self.content, Sanitize::Config.merge(Sanitize::Config::RELAXED,
				:add_attributes => {
					'a' => {'rel' => 'nofollow'}
				}
				# :elements        => Sanitize::Config::BASIC[:elements] + ['img', 'div', 'table', 'code', 'pre'],
				# :remove_contents => true
			))
		end

		def update_ticket
      self.ticket.touch(:last_reply_created_at)  
		end

		def follow_ticket
			if self.user.settings.follow_on_reply
			  self.user.follow(self.ticket)
			end
		end

		def set_ticket_feedback
			if self.user.is_agent?
			  self.ticket.needs_feedback = false
			else
			  self.ticket.needs_feedback = true
			end
			self.ticket.save
		end

end
