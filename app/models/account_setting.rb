class AccountSetting < ApplicationRecord
	belongs_to :account
	belongs_to :tickets_auto_assignee, ->() { agents }, class_name: 'User'
	has_attached_file :branding_logo, styles: { medium: "600x300>" }
  has_many :imports, :as => :assetable, :dependent => :destroy
	validates_attachment_content_type :branding_logo, content_type: ["image/jpeg", "image/jpg", "image/png", "image/gif"]
	validates_attachment_size :branding_logo, :less_than => 800.kilobytes
	before_save :filter_content
	store_accessor :oauth, 
		:envato_enabled, :envato_key, :envato_secret,
		:facebook_enabled, :facebook_key, :facebook_secret,
		:twitter_enabled, :twitter_key, :twitter_secret,
		:googleplus_enabled, :googleplus_key, :googleplus_secret

	def available_oauth_providers
		providers = []
		providers << 'facebook' if facebook_enabled
		providers << 'envato' if envato_enabled
		return providers
	end

  private
	  def filter_content
		  self.branding_custom_css = Sanitize::CSS.stylesheet(self.branding_custom_css, Sanitize::Config::RELAXED)
  	end
end
