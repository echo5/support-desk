class CannedResponse < ApplicationRecord
	acts_as_tenant(:account)
	validates_presence_of :name, :content
	before_save :filter_content

	def as_json(options={})
		ActiveSupport.escape_html_entities_in_json = true 
	  { 
	  	:name => self.name,
	  	:content => (self.content).html_safe 
	  }
	end


	private
		def filter_content
			self.content = Sanitize.fragment(self.content, Sanitize::Config::BASIC)
		end
end
