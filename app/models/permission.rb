class Permission < ApplicationRecord
  belongs_to :scheme, class_name: 'PermissionScheme', foreign_key: 'scheme_id', inverse_of: :permissions
  
  enum type: {
      role: 0,
      current_user: 1,
  }
  enum action: {
      'ticket.create': 0,
      'ticket.read': 1,
  }
end
