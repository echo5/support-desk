ThinkingSphinx::Index.define :ticket, :with => :real_time do
  # fields
  indexes subject
  indexes content

  has item_id, :type => :integer, :as => :item_id

end