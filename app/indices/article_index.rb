ThinkingSphinx::Index.define :article, :with => :real_time do
  # fields
  indexes title
  indexes content

  has item_ids, :type => :integer, :multi => true, :as => :item_id

end