class ImportWorker
  include Sidekiq::Worker

  def perform(type, args)
   @import = ImportUsers.new(args)
   @import.call
   abort @import.inspect
  end
end
