class AutoCloseTicketWorker
  include Sidekiq::Worker

  def perform(ticket_id, last_reply_created_at)
    @ticket = Ticket.find(ticket_id)
    if @ticket.last_reply_created_at == last_reply_created_at && !@ticket.needs_feedback && @ticket.status != 'closed'
			@ticket.status = 'closed'
			@ticket.save
  	end
  end
end
