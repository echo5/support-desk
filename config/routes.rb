require 'domains'

Rails.application.routes.draw do

  # Agents admin
  namespace :admin do            
    resource :account_settings, path: 'settings', :constraints => Subdomain do
      get :tickets
      get :articles
      get :canned_responses
      get :rules
      get :labels
      get :imports
      get :registration
      get :roles
      get :permission_schemes
      post :import_tickets
      post :import_users
      post :import_replies
    end
    resources :canned_responses, only: [:update, :create, :destroy, :edit], :constraints => Subdomain
    resources :rule_sets, :constraints => Subdomain
    resources :labels, only: [:update, :create, :destroy, :edit], :constraints => Subdomain
    resources :roles, only: [:update, :create, :destroy, :edit], :constraints => Subdomain do
      post :add_user, on: :collection
      post :remove_user, on: :collection
    end
    resources :permissions, :constraints => Subdomain
    resources :permission_schemes, :constraints => Subdomain
    resources :channel_items, :constraints => Subdomain, only: [:index, :update, :destroy], path: 'channels' do
      get :create_ticket, on: :member
      get :sync, on: :collection
    end
    resources :products, param: :slug, :constraints => Subdomain do
      get :branding, on: :member
      get :permissions, on: :member
      get :users, on: :member
      post :update_user_roles, on: :member
      get :envato, on: :member
    end
    resources :users, param: :username  do
      get :import, on: :collection
      get :sync_envato_purchases, on: :member
      resources :user_settings, path: 'settings', only: [:update, :create]
    end
    resources :categories, :constraints => Subdomain do
      # put :move_article, on: :member
      collection do
        match :sync, :via => [:get, :post]
      end
    end
    resources :tickets, :constraints => Subdomain do
      get :import, on: :collection
      get :follow, on: :member
      get :unfollow, on: :member
      patch :update_status, on: :member
      patch :assign, on: :member
      resources :replies, shallow: true
    end
    resources :articles, :constraints => Subdomain do
      post :move, on: :member
    end
    resources :sections, only: [:index, :new, :create, :update, :edit, :destroy, :show], :constraints => Subdomain do
      put :sort, on: :collection
      put :sort_articles, on: :member
      post :move, on: :member
    end
  end
  root to: "products#index", :as => 'categories_index', :constraints => Subdomain

  # Tenant frontend
  resources :tickets, :constraints => Subdomain  
  resources :media, only: [:update, :create, :destroy, :edit], :constraints => Subdomain
  resources :categories, only: [:show, :index], :constraints => Subdomain
  resources :articles #, :only => [:show], :constraints => Subdomain
  resources :search, only: [:index], :constraints => Subdomain
  devise_for :users, path: '', controllers: { sessions: "sessions", registrations: "registrations", :omniauth_callbacks => "users/omniauth_callbacks" }
  mount Payola::Engine => '/payola', as: :payola
  get 'pricing' => 'subscriptions#show', as: 'pricing'
  resource :account, only: [:new, :create]
  resource :subscription, only: [:show] do #, :constraints => RootDomain do
    # get :create
    post :cancel
    post :change_plan
    patch :update_card
    resources :subscriptions
  end
  mount StripeEvent::Engine => '/webhooks', :constraints => RootDomain
  resources :products, param: :slug, path: '/', only: [:index, :show], :constraints => Subdomain

  # Landing
  devise_for :admins
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin', :constraints => RootDomain
  root to: "pages#show", :slug => 'home', :constraints => RootDomain
  resources :posts, only: [:index, :show, :edit, :update], param: :slug, path: 'blog', :constraints => RootDomain
  resources :pages, only: [:show, :edit, :update], param: :slug, path: '', :constraints => RootDomain
  
end
