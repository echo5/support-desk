# include ApplicationHelper

# FACEBOOK_PROC = lambda do |env|
#   req = Rack::Request.new(env)
#   # Note client_id & client_secret for Facebook
#   account = Account.find_by_host(req.host)
#   if account
#     env['omniauth.strategy'].options[:client_id] = account.settings.facebook_app_id
#     env['omniauth.strategy'].options[:client_secret] = account.settings.facebook_app_secret
#   end
# end

# # Note the below block is different for a Rails app
# Rails.application.config.middleware.use OmniAuth::Builder do
#   provider :facebook, setup: FACEBOOK_PROC
#   provider :envato, ENV['ENVATO_CLIENT_ID'], ENV['ENVATO_API_KEY'] #client id
# end