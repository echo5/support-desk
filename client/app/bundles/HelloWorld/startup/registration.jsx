import ReactOnRails from 'react-on-rails';

import HelloWorld from '../components/HelloWorld';
import ProductsList from '../components/ProductsList';
import Product from '../components/Product';
import ReplyBox from '../components/ReplyBox';

// This is how react_on_rails can see the HelloWorld in the browser.
ReactOnRails.register({
  HelloWorld,
  ProductsList,
  Product,
  ReplyBox,
});
