import PropTypes from 'prop-types';
import React from 'react';

export default class Reply extends React.Component {
  static propTypes = {
    // reply: PropTypes.object.isRequired,
  };

  render() {
    return (
        <li className="media reply" id="reply-<%= reply.id %>">
            <div className="mr-3">
            {/* <%= raw reply.user.avatar %> */}
            </div>
            <div className="media-body">
                <div className="reply-meta h5">
                    { this.props.user }
                    {/* <%= reply.user.badge %>
                    <span class="text-muted reply-created-at ml-auto"><%= time_format reply.created_at %></span>
                    <div class="dropdown dropdown-reply">
                        <a class="nav-link dropdown-toggle" href="#" id="reply-dropdown-<%= reply.id %>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="material-icons">more_horiz</i></a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="reply-dropdown-<%= reply.id %>">
                            <%= link_to raw('<i class="material-icons">link</i> Permalink'), ticket_path(@ticket.id) + "\#reply-#{reply.id}", :class => 'dropdown-item permalink' %>
                            <% if can? :edit, reply %>
                            <%= link_to raw('<i class="material-icons">mode_edit</i> Edit'), edit_reply_path(reply.id), :class => 'dropdown-item' %>
                            <%= link_to raw('<i class="material-icons">delete</i> Delete'), reply_path(reply.id), :class => 'dropdown-item', :remote => true, method: :delete %>
                            <% end %>
                        </div>
                    </div> */}
                </div>

                <div className="reply-content" dangerouslySetInnerHTML={{__html: this.props.content}} />

                <div className="attachments">
                    {/* <% reply.attachments.each do |attachment| %>
                    <%= link_to image_tag(attachment.url(:thumb)), attachment.url, target: :blank %>
                    <% end %> */}
                </div>
            </div>
        </li>
    );
  }
}
