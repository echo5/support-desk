import PropTypes from 'prop-types';
import React from 'react';

export default class Product extends React.Component {
  static propTypes = {
    product: PropTypes.object.isRequired, // this is passed from the Rails view
  };

  render() {
    return (
      <div>
        {this.props.product.name}
      </div>
    );
  }
}
