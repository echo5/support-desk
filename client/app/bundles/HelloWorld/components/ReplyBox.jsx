import PropTypes from 'prop-types';
import React from 'react';
import ReplyList from '../components/ReplyList';
import ReplyForm from '../components/ReplyForm';


export default class ReplyBox extends React.Component {

  constructor(props){
    super(props);
    this.state = { replies: this.props.replies };
    this.handleReplySubmit = this.handleReplySubmit.bind(this);
  }

  handleReplySubmit(form) {

    var replies = this.state.replies;
    // Optimistically set an id on the new comment. It will be replaced by an
    // id generated by the server. In a production application you would likely
    // not use Date.now() for this and would have a more robust system in place.
    form.reply.created_at = Date.now();
    var newReplies = replies.concat([form.reply]);
    this.setState({replies: newReplies})
    
    function handleErrors(response) {
        if (!response.ok) {
            addToast(response.statusText, 'error');
            throw Error(response.statusText);
        }
        return response;
    }
    let header = ReactOnRails.authenticityHeaders({'Content-Type': 'application/json'});
    fetch(this.props.form.action, {
        method: 'POST',
        body: JSON.stringify(form),
        headers: header,
        credentials: 'same-origin'
    }).then(handleErrors)
        .then(response => console.log("ok") )
        .catch(error => console.log(error) );
    
    // $.ajax({
    //   url: this.props.form.action,
    //   dataType: 'json',
    //   type: 'POST',
    //   data: { reply: reply },
    //   success: function(data) {
    //     // this.setState({data: data});
    //   }.bind(this),
    //   error: function(xhr, status, err) {
    //     this.setState({replies: replies});
    //     console.log(status);
    //     // console.error(this.props.url, status, err.toString());
    //   }.bind(this)
    // });

  }

  render  () {
    return (
      <div className="replies-box">
        <ReplyList replies={this.state.replies} />
        <ReplyForm action={this.props.form.action} onReplySubmit={this.handleReplySubmit} />
      </div>
    )
  }
};