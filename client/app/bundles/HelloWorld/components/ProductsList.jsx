import PropTypes from 'prop-types';
import React from 'react';
import Product from '../components/product';

export default class ProductsList extends React.Component {
  static propTypes = {
    products: PropTypes.array.isRequired, // this is passed from the Rails view
  };

  /**
   * @param props - Comes from your rails view.
   * @param _railsContext - Comes from React on Rails
   */
  constructor(props, _railsContext) {
    super(props);
    this.state = { products: this.props.products };
}

render() {
    let products = this.state.products.map(function(product, index) {
      return (
        <Product product={product} key={index}></Product>
      );
    });
    return (
      <div>
        Hi
        {products}
      </div>
    );
  }
}
