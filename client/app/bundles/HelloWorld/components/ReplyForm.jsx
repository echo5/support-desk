import PropTypes from 'prop-types';
import React from 'react';

export default class Reply extends React.Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(e) {
    e.preventDefault();

    var content = this.refs.content.value.trim();

    // validate
    if (!content) {
      return false;
    }

    // submit
    this.props.onReplySubmit({reply: {content: content} });

    // reset form
    this.refs.content.value = "";
  }

  render  () {
    return (
      <form ref="form" className="reply-form" onSubmit={ this.handleSubmit }>
        <p><textarea ref="content" name="reply[content]" placeholder="Your name" /></p>
        <p><button type="submit">Reply</button></p>
      </form>
    )
  }
};