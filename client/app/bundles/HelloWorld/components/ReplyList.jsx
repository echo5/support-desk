import PropTypes from 'prop-types';
import React from 'react';
import Reply from '../components/reply';

export default class ReplyList extends React.Component {
  static propTypes = {
    replies: PropTypes.array.isRequired,
  };

  /**
   * @param props - Comes from your rails view.
   * @param _railsContext - Comes from React on Rails
   */
  constructor(props, _railsContext) {
    super(props);
  }

render() {
    let replies = this.props.replies.map(function(reply, index) {
      return (
        <Reply {...reply} key={index}></Reply>
      );
    });
    return (
      <ul className="replies">
        Hi
        {replies}
      </ul>
    );
  }
}
