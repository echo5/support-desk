class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.string :slug
      t.attachment :image
      t.integer :status, :default => 1, :null => false
      t.jsonb :info
      t.references :account, index: true

      t.timestamps
    end
  end
end
