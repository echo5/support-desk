class CreateChannelItems < ActiveRecord::Migration[5.0]
  def change
    create_table :channel_items do |t|
    	t.integer :external_id
    	t.integer :channel
    	t.references :product, index: true
    	t.references :ticket
    	t.string :username
    	t.string :avatar_url
    	t.text :content
    	t.datetime :last_reply_created_at
      t.references :account, index: true

      t.timestamps
    end
  end
end
