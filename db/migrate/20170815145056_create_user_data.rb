class CreateUserData < ActiveRecord::Migration[5.0]
  def change
    create_table :user_data do |t|
      t.references :user, index: true
      t.string :key
      t.jsonb :info

      t.timestamps
    end
  end
end
