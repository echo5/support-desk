class CreateSubscriptions < ActiveRecord::Migration[5.0]
  def change
    create_table :subscriptions do |t|
      t.string :stripe_customer_id
      t.integer :plan_id
      t.integer :quantity
      t.integer :coupon_id
      t.string :card_last4
      t.date :card_expiration
      t.string :card_type
      t.float :current_price
      t.integer :account_id
      t.datetime :expires_at

      t.timestamp :start
      t.string :status
      t.boolean :cancel_at_period_end
      t.timestamp :current_period_start
      t.timestamp :current_period_end
      t.timestamp :ended_at
      t.timestamp :trial_start
      t.timestamp :trial_end
      t.timestamp :canceled_at
      t.integer :quantity
      t.string   :stripe_id
      t.string   :stripe_token
      t.text     :error
      t.string   :state

      t.timestamps
    end
  end
end
