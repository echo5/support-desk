class CreateJoinTableItemsRuleSets < ActiveRecord::Migration[5.0]
  def change
    create_join_table :items, :rule_sets do |t|
      t.index [:item_id, :rule_set_id]
      # t.index [:rule_set_id, :item_id]
    end
  end
end
