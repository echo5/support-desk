class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
		t.string :title
		t.string :slug
		t.text :description
		t.attachment :image
		t.text :content
		t.references :user, index: true, foreign_key: true
		t.integer :status, :default => 1, :null => false

      t.timestamps
    end
  end
end
