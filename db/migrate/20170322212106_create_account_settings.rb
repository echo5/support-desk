class CreateAccountSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :account_settings do |t|
      t.references :account
      t.string :branding_company_name
      t.attachment :branding_logo
      t.string :branding_header_color
      t.string :branding_header_font_color
      t.string :branding_primary_color
      t.string :branding_secondary_color
      t.string :branding_domain
      t.text :branding_custom_css
      t.boolean :tickets_require_valid_support, default: true
      t.boolean :tickets_auto_close, default: false
      t.integer :tickets_auto_close_days, default: 5
      t.boolean :tickets_private, default: false
      t.references :tickets_auto_assignee, references: :users
      t.boolean :articles_private, default: false
      t.jsonb, :oauth, default: '{}'

      t.timestamps
    end
  end
end
