class CreateCannedResponses < ActiveRecord::Migration[5.0]
  def change
    create_table :canned_responses do |t|
      t.string :name
      t.text :content
      t.references :account, index: true

      t.timestamps
    end
  end
end
