class CreateTickets < ActiveRecord::Migration[5.0]
  def change
    create_table :tickets do |t|
      t.string :subject
      t.references :user, references: :users
      t.references :assignee, references: :users
      t.references :product
      t.integer :status, :default => 0, :null => false
      t.boolean :needs_feedback, :default => true
      t.datetime :last_reply_created_at, :default => Time.now
      t.references :account, index: true
      t.integer :external_id

      t.timestamps
    end
  end
end
