class CreateMedia < ActiveRecord::Migration[5.0]
  def change
    create_table :media do |t|
    	t.string   :type
    	t.integer  :attachable_id
    	t.string   :attachable_type
	  	t.attachment :attachment
      t.references :account, index: true

      t.timestamps
    end
  end
end
