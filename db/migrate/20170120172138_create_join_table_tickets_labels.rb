class CreateJoinTableTicketsLabels < ActiveRecord::Migration[5.0]
  def change
    create_join_table :tickets, :labels do |t|
      t.index [:ticket_id, :label_id]
      # t.index [:label_id, :ticket_id]
    end
  end
end
