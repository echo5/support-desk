class CreatePermissions < ActiveRecord::Migration[5.0]
  def change
    create_table :permissions do |t|
      t.references :scheme, index: true, foreign_key: { to_table: :permission_schemes }
      t.integer :action, index: true
      t.integer :type
      t.jsonb :value

      t.timestamps
    end
  end
end
