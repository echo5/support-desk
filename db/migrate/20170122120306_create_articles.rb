class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :articles do |t|
      t.string :title
      t.text :content
      t.integer :position
      t.references :section, foreign_key: true
      t.integer :status, :default => 1, :null => false
      t.references :account, index: true

      t.timestamps
    end
  end
end
