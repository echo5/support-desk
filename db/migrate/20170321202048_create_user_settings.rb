class CreateUserSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :user_settings do |t|
      t.references :user
      t.boolean :receive_emails, :default => true
      t.boolean :follow_on_reply, :default => true
      t.boolean :follow_on_assign, :default => true
      t.boolean :follow_all, :default => false

      t.timestamps
    end
  end
end
