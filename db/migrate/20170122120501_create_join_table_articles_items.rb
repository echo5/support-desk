class CreateJoinTableArticlesItems < ActiveRecord::Migration[5.0]
  def change
    create_join_table :articles, :items do |t|
      t.integer :id, :primary_key
      t.index [:category_id, :article_id]
      # t.index [:article_id, :item_id]
    end
  end
end
