class CreateSections < ActiveRecord::Migration[5.0]
  def change
    create_table :sections do |t|
      t.string :name
      t.integer :position
      t.references :category, foreign_key: true
      t.references :account, index: true

      t.timestamps
    end
  end
end
