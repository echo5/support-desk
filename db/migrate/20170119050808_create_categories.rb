class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
      t.string :name
      t.string :slug
      t.attachment :image
      t.integer :status, :default => 1, :null => false
      t.references :product, index: true
      t.references :account, index: true
      
      t.timestamps
    end
  end
end
