class CreateReplies < ActiveRecord::Migration[5.0]
  def change
    create_table :replies do |t|
      t.text :content
      t.references :ticket
      t.references :user
      t.boolean :is_note
      t.references :account, index: true
      t.integer :external_id
        
      t.timestamps
    end
  end
end
