class CreateAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :accounts do |t|
      t.string :subdomain
      t.references :owner, references: :user

      t.timestamps
    end
  end
end
