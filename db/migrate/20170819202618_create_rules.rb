
class CreateRules < ActiveRecord::Migration
  def change
    create_table :rules do |t|
      t.belongs_to :rule_set
      t.string :model
      t.string :action
      t.string :left
      t.string :right
      t.string :operator

      t.timestamps
    end
  end
end
