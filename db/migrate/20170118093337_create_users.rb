class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
		  t.string :name
			t.string :username
			t.attachment :avatar			
		  t.integer :role, :default => 0
	    t.references :account, index: true
	    t.integer :external_id

	    t.timestamps
    end
  end
end
