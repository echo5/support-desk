# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create([
 {
 	name: 'Joshua',
 	email: 'joshua@echo5web.com',
 	password: 'oca6rina',
 	password_confirmation: 'oca6rina',
 },
 {
 	name: 'Morgana',
 	email: 'morgana@echo5web.com',
 	password: 'm4t86m06',
 	password_confirmation: 'm4t86m06',
 },
])

connection = ActiveRecord::Base.connection()
connection.execute("INSERT INTO `items` (`id`, `name`, `envato_id`, `created_at`, `updated_at`)
VALUES
	(1, 'Fusionate - Retina Multi-Purpose WordPress Theme', 6938295, '2017-01-18 15:12:31', '2017-01-18 15:12:31'),
	(2, 'Max - Retina One Page WordPress Theme', 7080460, '2017-01-18 15:12:31', '2017-01-18 15:12:31'),
	(3, 'QuickStep - Responsive One Page Portfolio Theme', 3237426, '2017-01-18 15:12:31', '2017-01-18 15:12:31'),
	(4, 'Etch - Clean Multi-Purpose WordPress Theme', 16846906, '2017-01-18 15:12:31', '2017-01-18 15:12:31'),
	(5, 'Glissando - Creative One Page Multipurpose Theme', 7504395, '2017-01-18 15:12:31', '2017-01-18 15:12:31'),
	(6, 'SimpleFlex - Flat One Page WordPress Theme', 7247149, '2017-01-18 15:12:31', '2017-01-18 15:12:31'),
	(7, 'Miinus - Retina Responsive Multi-Purpose Theme', 6278484, '2017-01-18 15:12:31', '2017-01-18 15:12:31');")
