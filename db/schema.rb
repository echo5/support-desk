# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170914092515) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "account_settings", force: :cascade do |t|
    t.integer  "account_id"
    t.string   "branding_company_name"
    t.string   "branding_logo_file_name"
    t.string   "branding_logo_content_type"
    t.integer  "branding_logo_file_size"
    t.datetime "branding_logo_updated_at"
    t.string   "branding_header_color"
    t.string   "branding_header_font_color"
    t.string   "branding_primary_color"
    t.string   "branding_secondary_color"
    t.text     "branding_custom_css"
    t.boolean  "tickets_require_valid_support", default: true
    t.boolean  "tickets_auto_close",            default: false
    t.integer  "tickets_auto_close_days",       default: 5
    t.boolean  "tickets_private",               default: false
    t.integer  "tickets_auto_assignee_id"
    t.boolean  "articles_private",              default: false
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.string   "branding_domain"
    t.string   "facebook_app_id"
    t.string   "facebook_app_secret"
    t.jsonb    "oauth"
    t.index ["account_id"], name: "index_account_settings_on_account_id", using: :btree
    t.index ["tickets_auto_assignee_id"], name: "index_account_settings_on_tickets_auto_assignee_id", using: :btree
  end

  create_table "accounts", force: :cascade do |t|
    t.string   "subdomain"
    t.integer  "owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_id"], name: "index_accounts_on_owner_id", using: :btree
  end

  create_table "admins", force: :cascade do |t|
    t.string   "username"
    t.integer  "role",                   default: 0
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_admins_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree
  end

  create_table "articles", force: :cascade do |t|
    t.string   "title"
    t.text     "content"
    t.integer  "position"
    t.integer  "section_id"
    t.integer  "account_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "status",     default: 1
    t.index ["account_id"], name: "index_articles_on_account_id", using: :btree
    t.index ["section_id"], name: "index_articles_on_section_id", using: :btree
  end

  create_table "articles_items", id: false, force: :cascade do |t|
    t.integer "article_id",  null: false
    t.integer "item_id",     null: false
    t.integer "id"
    t.integer "primary_key"
    t.index ["item_id", "article_id"], name: "index_articles_items_on_item_id_and_article_id", using: :btree
  end

  create_table "canned_responses", force: :cascade do |t|
    t.string   "name"
    t.text     "content"
    t.integer  "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_canned_responses_on_account_id", using: :btree
  end

  create_table "categories", id: :integer, default: -> { "nextval('items_id_seq'::regclass)" }, force: :cascade do |t|
    t.string   "name"
    t.string   "slug"
    t.integer  "product_id"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "account_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "source"
    t.integer  "status",             default: 1
    t.index ["account_id"], name: "index_items_on_account_id", using: :btree
  end

  create_table "channel_items", force: :cascade do |t|
    t.integer  "external_id"
    t.integer  "product_id"
    t.integer  "ticket_id"
    t.integer  "source"
    t.string   "username"
    t.string   "avatar_url"
    t.text     "content"
    t.datetime "last_reply_created_at"
    t.integer  "account_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["account_id"], name: "index_channel_items_on_account_id", using: :btree
    t.index ["product_id"], name: "index_channel_items_on_item_id", using: :btree
    t.index ["ticket_id"], name: "index_channel_items_on_ticket_id", using: :btree
  end

  create_table "follows", force: :cascade do |t|
    t.string   "followable_type",                 null: false
    t.integer  "followable_id",                   null: false
    t.string   "follower_type",                   null: false
    t.integer  "follower_id",                     null: false
    t.boolean  "blocked",         default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["followable_id", "followable_type"], name: "fk_followables", using: :btree
    t.index ["follower_id", "follower_type"], name: "fk_follows", using: :btree
  end

  create_table "identities", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.string   "access_token"
    t.string   "refresh_token"
    t.datetime "expires_at"
    t.integer  "account_id"
  end

  create_table "import_references", force: :cascade do |t|
    t.integer  "previous_id"
    t.string   "importable_type"
    t.integer  "importable_id"
    t.integer  "account_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["account_id"], name: "index_import_references_on_account_id", using: :btree
    t.index ["importable_type", "importable_id"], name: "index_import_references_on_importable_type_and_importable_id", using: :btree
    t.index ["previous_id"], name: "index_import_references_on_previous_id", using: :btree
  end

  create_table "items_rule_sets", id: false, force: :cascade do |t|
    t.integer "item_id",     null: false
    t.integer "rule_set_id", null: false
    t.index ["item_id", "rule_set_id"], name: "index_items_rule_sets_on_item_id_and_rule_set_id", using: :btree
  end

  create_table "labels", force: :cascade do |t|
    t.string   "name"
    t.string   "color"
    t.integer  "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_labels_on_account_id", using: :btree
  end

  create_table "labels_tickets", id: false, force: :cascade do |t|
    t.integer "ticket_id", null: false
    t.integer "label_id",  null: false
    t.index ["ticket_id", "label_id"], name: "index_labels_tickets_on_ticket_id_and_label_id", using: :btree
  end

  create_table "media", id: :integer, default: -> { "nextval('assets_id_seq'::regclass)" }, force: :cascade do |t|
    t.string   "type"
    t.integer  "attachable_id"
    t.string   "attachable_type"
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.integer  "account_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["account_id"], name: "index_assets_on_account_id", using: :btree
  end

  create_table "pages", force: :cascade do |t|
    t.string   "title"
    t.string   "slug"
    t.string   "description"
    t.text     "content"
    t.boolean  "is_published"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payola_affiliates", force: :cascade do |t|
    t.string   "code"
    t.string   "email"
    t.integer  "percent"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payola_coupons", force: :cascade do |t|
    t.string   "code"
    t.integer  "percent_off"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "active",      default: true
  end

  create_table "payola_sales", force: :cascade do |t|
    t.string   "email",                limit: 191
    t.string   "guid",                 limit: 191
    t.integer  "product_id"
    t.string   "product_type",         limit: 100
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "state"
    t.string   "stripe_id"
    t.string   "stripe_token"
    t.string   "card_last4"
    t.date     "card_expiration"
    t.string   "card_type"
    t.text     "error"
    t.integer  "amount"
    t.integer  "fee_amount"
    t.integer  "coupon_id"
    t.boolean  "opt_in"
    t.integer  "download_count"
    t.integer  "affiliate_id"
    t.text     "customer_address"
    t.text     "business_address"
    t.string   "stripe_customer_id",   limit: 191
    t.string   "currency"
    t.text     "signed_custom_fields"
    t.integer  "owner_id"
    t.string   "owner_type",           limit: 100
    t.index ["coupon_id"], name: "index_payola_sales_on_coupon_id", using: :btree
    t.index ["email"], name: "index_payola_sales_on_email", using: :btree
    t.index ["guid"], name: "index_payola_sales_on_guid", using: :btree
    t.index ["owner_id", "owner_type"], name: "index_payola_sales_on_owner_id_and_owner_type", using: :btree
    t.index ["product_id", "product_type"], name: "index_payola_sales_on_product", using: :btree
    t.index ["stripe_customer_id"], name: "index_payola_sales_on_stripe_customer_id", using: :btree
  end

  create_table "payola_stripe_webhooks", force: :cascade do |t|
    t.string   "stripe_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payola_subscriptions", force: :cascade do |t|
    t.string   "plan_type"
    t.integer  "plan_id"
    t.datetime "start"
    t.string   "status"
    t.string   "owner_type"
    t.integer  "owner_id"
    t.string   "stripe_customer_id"
    t.boolean  "cancel_at_period_end"
    t.datetime "current_period_start"
    t.datetime "current_period_end"
    t.datetime "ended_at"
    t.datetime "trial_start"
    t.datetime "trial_end"
    t.datetime "canceled_at"
    t.integer  "quantity"
    t.string   "stripe_id"
    t.string   "stripe_token"
    t.string   "card_last4"
    t.date     "card_expiration"
    t.string   "card_type"
    t.text     "error"
    t.string   "state"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "currency"
    t.integer  "amount"
    t.string   "guid",                 limit: 191
    t.string   "stripe_status"
    t.integer  "affiliate_id"
    t.string   "coupon"
    t.text     "signed_custom_fields"
    t.text     "customer_address"
    t.text     "business_address"
    t.integer  "setup_fee"
    t.decimal  "tax_percent",                      precision: 4, scale: 2
    t.index ["guid"], name: "index_payola_subscriptions_on_guid", using: :btree
  end

  create_table "permission_schemes", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "permissions", force: :cascade do |t|
    t.integer  "scheme_id_id"
    t.integer  "action"
    t.integer  "type"
    t.jsonb    "value"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["action"], name: "index_permissions_on_action", using: :btree
    t.index ["scheme_id_id"], name: "index_permissions_on_scheme_id_id", using: :btree
  end

  create_table "plans", force: :cascade do |t|
    t.string   "name"
    t.string   "stripe_id"
    t.float    "price"
    t.string   "interval"
    t.text     "features"
    t.boolean  "highlight"
    t.integer  "display_order"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "posts", force: :cascade do |t|
    t.string   "title"
    t.string   "slug"
    t.text     "description"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.text     "content"
    t.integer  "user_id"
    t.boolean  "is_published"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["user_id"], name: "index_posts_on_user_id", using: :btree
  end

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "slug"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "status",             default: 1, null: false
    t.jsonb    "info"
    t.integer  "account_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.index ["account_id"], name: "index_products_on_account_id", using: :btree
  end

  create_table "replies", force: :cascade do |t|
    t.text     "content"
    t.integer  "ticket_id"
    t.integer  "user_id"
    t.boolean  "is_published"
    t.integer  "account_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["account_id"], name: "index_replies_on_account_id", using: :btree
    t.index ["ticket_id"], name: "index_replies_on_ticket_id", using: :btree
    t.index ["user_id"], name: "index_replies_on_user_id", using: :btree
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "account_id"
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
    t.index ["name"], name: "index_roles_on_name", using: :btree
  end

  create_table "rule_sets", force: :cascade do |t|
    t.string   "name"
    t.string   "matching_logic"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rules", force: :cascade do |t|
    t.integer  "rule_set_id"
    t.string   "model"
    t.string   "action"
    t.string   "user_key"
    t.string   "model_key"
    t.string   "operator"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "user_condition"
  end

  create_table "sections", force: :cascade do |t|
    t.string   "name"
    t.integer  "position"
    t.integer  "category_id"
    t.integer  "account_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["account_id"], name: "index_sections_on_account_id", using: :btree
    t.index ["category_id"], name: "index_sections_on_item_id", using: :btree
  end

  create_table "subscription_plans", force: :cascade do |t|
    t.integer  "amount"
    t.string   "interval"
    t.string   "stripe_id"
    t.string   "name"
    t.text     "features"
    t.boolean  "highlight"
    t.integer  "display_order"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "subscriptions", force: :cascade do |t|
    t.string   "stripe_customer_id"
    t.integer  "plan_id"
    t.integer  "quantity"
    t.integer  "coupon_id"
    t.string   "card_last4"
    t.date     "card_expiration"
    t.string   "card_type"
    t.float    "current_price"
    t.integer  "account_id"
    t.datetime "expires_at"
    t.datetime "start"
    t.string   "status"
    t.boolean  "cancel_at_period_end"
    t.datetime "current_period_start"
    t.datetime "current_period_end"
    t.datetime "ended_at"
    t.datetime "trial_start"
    t.datetime "trial_end"
    t.datetime "canceled_at"
    t.string   "stripe_id"
    t.string   "stripe_token"
    t.text     "error"
    t.string   "state"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "tickets", force: :cascade do |t|
    t.string   "subject"
    t.text     "content"
    t.integer  "user_id"
    t.integer  "assignee_id"
    t.integer  "product_id"
    t.integer  "purchase_id"
    t.integer  "status",                default: 0,                     null: false
    t.integer  "priority"
    t.boolean  "needs_feedback",        default: true
    t.datetime "last_reply_created_at", default: '2017-07-18 15:40:11'
    t.integer  "account_id"
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.integer  "external_id"
    t.index ["account_id"], name: "index_tickets_on_account_id", using: :btree
    t.index ["assignee_id"], name: "index_tickets_on_assignee_id", using: :btree
    t.index ["product_id"], name: "index_tickets_on_item_id", using: :btree
    t.index ["purchase_id"], name: "index_tickets_on_purchase_id", using: :btree
    t.index ["user_id"], name: "index_tickets_on_user_id", using: :btree
  end

  create_table "user_data", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "key"
    t.jsonb    "info"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_data_on_user_id_id", using: :btree
  end

  create_table "user_settings", force: :cascade do |t|
    t.integer  "user_id"
    t.boolean  "receive_emails",   default: true
    t.boolean  "follow_on_reply",  default: true
    t.boolean  "follow_on_assign", default: true
    t.boolean  "follow_all",       default: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.index ["user_id"], name: "index_user_settings_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "username"
    t.string   "envato_username"
    t.integer  "role",                   default: 0
    t.integer  "channel"
    t.string   "envato_code"
    t.string   "envato_refresh_token"
    t.string   "envato_access_token"
    t.datetime "envato_expires_in"
    t.integer  "account_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "external_id"
    t.index ["account_id"], name: "index_users_on_account_id", using: :btree
    t.index ["email"], name: "index_users_on_email", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "expires_at"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree
  end

  add_foreign_key "articles", "sections"
  add_foreign_key "permissions", "permission_schemes", column: "scheme_id_id"
  add_foreign_key "posts", "users"
  add_foreign_key "sections", "categories"
end
