source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.1'
# Dotenv
gem 'dotenv-rails', :groups => [:development, :test, :production], :require => 'dotenv/rails-now'
# Use mysql as the database for Active Record
gem 'mysql2', '>= 0.3.13', '< 0.5'
gem 'pg'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# Use SCSS for stylesheets
gem 'sassc-rails'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'
gem "react_on_rails", "8.0.0"

# authentication
gem 'devise'
gem 'omniauth'
gem 'omniauth-facebook'
gem 'omniauth-twitter'
gem 'omniauth-gplus'

# authorization
gem 'pundit'
gem 'cancancan', "~> 1.15"
gem 'rolify'

# pagination
gem 'will_paginate', "~> 3.1"

# attachments, thumbnails etc
gem 'paperclip', "~> 5.1"
gem 'dropzonejs-rails'

# tags labels
gem 'acts-as-taggable-on', '~> 4.0'

# Multi tenancy
# gem 'apartment'
gem 'acts_as_tenant'

# Bootstrap
gem 'bootstrap', '~> 4.0.0.alpha6'
gem 'tether-rails'

# Simple form
gem 'simple_form'
gem 'cocoon'
gem 'enum_help'
gem 'trix'

# Content filter
gem 'sanitize'
gem 'nokogiri'
gem 'dentaku'

# Searching
gem 'thinking-sphinx'

# Followers
gem 'acts_as_follower', github: 'tcocca/acts_as_follower', branch: 'master'

# Sorting/order
gem 'activerecord-sortable'
gem 'jquery-ui-rails'
gem 'ranked-model'

# Selectize
gem "selectize-rails"

# Payments and subscriptions
gem 'payola-payments'
gem 'stripe'
gem 'stripe_event'

# Importing
gem 'activerecord-import'

# Delayed jobs
gem 'sidekiq'
gem 'delayed_job_active_record'
gem 'daemons'

# Frontend admin
gem 'rails_admin', '~> 1.2'

source 'https://rails-assets.org' do
  gem 'rails-assets-jquery-minicolors'
  gem 'rails-assets-sortablejs'
  gem 'rails-assets-html.sortable'
end

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  # gem 'byebug', platform: :mri
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  # Live reload
  gem "guard", ">= 2.2.2", :require => false
  gem "guard-livereload",  :require => false
  gem "rack-livereload"
  gem "rb-fsevent",        :require => false
  # Readline
  gem "rb-readline"
  gem "ruby-debug-ide"
  gem "debase"
  gem "foreman"
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

gem 'mini_racer', platforms: :ruby
gem 'webpacker_lite'
